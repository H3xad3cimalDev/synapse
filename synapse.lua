// Synapse (dotGScript v2). Created by H3xad3cimal. You can contact me on discord at H3xad3cimal#0139
// Parameter Check
if params.len < 1 then exit("Synapse is developed and created by H3xad3cimal#0139. (GitLab: https://gitlab.com/H3xad3cimalDev/synapse/)\n<B>Usage: synapse <argument> <option>\nHelp: synapse help") end if

// Quick Setup Variables
sShell = get_shell
cComputer = sShell.host_computer

sSynapsePath = "/usr/synapse/"
sDefaultMain = "main = function(args)" + char(10) + char(9) + "print(""Hello World!"")" + char(10) + "end function" + char(10) + char(10) + "main(params)"

String2CharArray = function(str)
	lArray = []

	for c in str
		lArray.push(c)
	end for

	return lArray
end function

// Base Util Function
// Someone optimise this function when I upload to github pls
SplitUntil = function(s0, sSplitChar, nSplitUntil)
	lList = []
	sFinal = ""
	sSplit = ""
	
	for cChar in s0
		if lList.len == nSplitUntil - 1 then
			sFinal = sFinal + cChar
		else
			if cChar == sSplitChar then
				lList.push(sSplit)
				sSplit = ""
				continue
			end if
			
			sSplit = sSplit + cChar
		end if
	end for
	
	lList.push(sFinal)
	
	return lList
end function

DebugBreak = function()
	user_input("", false, true)
end function

HashString = function(sString)
	if sString == null then
		return ""
	end if
	return md5(sString)
end function

HashFile = function(fFile) 
	return HashString(ShortenCodeString(fFile.get_content))
end function

ConvertToHex = function(num)
	if num == 0 then
		return "00"
	end if
	if num < 0 then
		num = num * -1
	end if
	hexstr = "0123456789ABCDEF"
	result = ""
	while true
		if not num > 0 then break end if
		n = num % 16
		result = slice(hexstr, n, n + 1) + result
		num = floor(num / 16)
	end while
	if result.len == 1 then
		result = "0" + result
	end if
	return result
end function


IsNumber = function(str)
    if typeof(str.to_int) == "number" then
        return true
    end if

    return false
end function

rgb = function(r, g, b)
	return "#" + ConvertToHex(r) + ConvertToHex(g) + ConvertToHex(b)
end function

GetFile = function(sPath)
	fFile = cComputer.File( sPath )

	if not fFile then
		fFile = cComputer.File( current_path + sPath )

		if not fFile then return null end if
	end if

	return fFile
end function

FixPath = function(sPath)
	sParentPath = parent_path( sPath )

	if sParentPath == sPath then
		return current_path + "/" + sParentPath
	end if

	return sPath
end function

path_file = function(sPath)
	return sPath.split("/").pop()
end function

mew = function(oOrginal)
	oClone = null

	if typeof(oOrginal) == "list" then
		oClone = []

		for v in oOrginal
			if typeof(@v) == "function" then
				oClone.push(@v)
			else if typeof(v) == "map" or typeof(v) == "list" then
				oClone.push(mew(v))
			else
				oClone.push(v)
			end if
		end for
	else if typeof(oOrginal) == "map" then
		oClone = {}

		for v in oOrginal
			if typeof(@v.value) == "function" then
				oClone[v.key] = @v.value
			else if typeof(v.value) == "map" or typeof(v) == "list" then
				oClone[v.key] = mew(v.value)
			else
				oClone[v.key] = v.value
			end if
		end for
	end if

	return oClone
end function

ColorCast = function(color, message)
	return "<color=" + color + ">" + message + "</color>"
end function

BoldCast = function(message)
	return "<b>" + message + "</b>"
end function

Exception = function(message)
	exit("<b>" + ColorCast("#ff0000", "Thrown Exception: " + message))
end function

GetHashOfFile = function(fFile)
end function

PermissionCheck = function(file, permissions)
	for permission in permissions
		if not file.has_permission(permission) then
			return false
		end if
	end for
	return true
end function

cSpace = char(900)
cformat_columns = function(salida)
	separacion = 2
	strArray1 = salida.split(char(10))
	stringListList = []
	intList = []

	for index1 in range(0, strArray1.len - 1)
		strArray2 = strArray1[index1].split(" ")
		stringListList.push([])
		for index2 in range(0, strArray2.len - 1)
			if strArray2.len > intList.len then
				intList.push(index2)
			end if
			str = strArray2[index2]
			if strArray2[index2].len > intList[index2] then
				intList[index2] = strArray2[index2].len
			end if
			stringListList[index1].push(str.trim())
		end for
	end for
	str1 = ""
	for index3 in range(0, stringListList.len - 1)
		for index4 in range(0, stringListList[index3].len - 1)
			str1 = str1 + stringListList[index3][index4].replace(cSpace, " ")
			num = intList[index4] - stringListList[index3][index4].len + separacion
			for index5 in range(0, num - 1)
				str1 = str1 + " "
			end for
		end for
		if index3 < stringListList.len - 1 then
			str1 = str1 + char(10)
		end if
	end for
	return str1
end function

StringContains = function(s0, s1)
	return not s0.indexOf(s1) == null
end function

IndexesOf = function(s0, s1)
	lIndexes = []
	nRIndex = 0

	for indx in range(0, s0.len - 1)
		indx = nRIndex
		nEndIndx = indx + s1.len
		if nEndIndx > s0.len then
			break
		end if

		sStr = slice(s0, indx, nEndIndx)
		if sStr == s1 then
			lIndexes.push(indx)
			nRIndex = nEndIndx
			continue
		end if

		nRIndex = indx + 1
	end for

	return lIndexes
end function

//ShortenCodeString = function(sCode)
//	lCodeLines = sCode.split(char(10))
//	sFinalCode = ""
//
//	for sLine in lCodeLines
//        sLine = sLine.trim()
//		if sLine == "" then
//			continue
//		end if
//
//		if sLine.len > 1 then
//			if slice(sLine, 0, 2) == "//" then
//				continue
//			end if
//		end if
//
//		if StringContains(sLine, "//") then
//			// strip comments
//			lQuotes   = IndexesOf(sLine, """")
//			lComments = IndexesOf(sLine, "//")
//
//			if lQuotes.len < 2 then
//				sLine = slice(sLine, 0, lComments.pull())
//			else
//				// if this is left as -1 that means there are no good comment symbols
//				nCommentLine = -1
//
//				for nCommentIndx in lComments
//					if lQuotes.len < 2 and lComments.len > 0 then
//						nCommentLine = lComments.pull()
//						break
//					end if
//
//					nLeftQuote = lQuotes.pull()
//					nRightQuote = lQuotes.pull()
//
//					if nLeftQuote < nCommentIndx and nRightQuote > nCommentIndx then
//						continue
//					end if
//
//					nCommentLine = nCommentIndx
//					break
//				end for
//
//				if not nCommentLine == -1 then
//					sLine = slice(sLine, 0, nCommentLine)
//				end if
//			end if
//		end if
//
//		sFinalCode = sFinalCode + sLine + char(10)
//	end for
//
//	return slice(sFinalCode, 0, sFinalCode.len - 1)
//end function

RunCodeProcessorOnFile = function(fFile, bShorten, bPreprocess)
	cpCodeProcessor = new CodeProcessor
	cpCodeProcessor.InitFile(fFile, bShorten, bPreprocess)
	return cpCodeProcessor.Generate()
end function

ShortenCodeFile = function(fFile)
	return RunCodeProcessorOnFile(fFile, true, false)
end function

ProcessCodeFile = function(fFile)
	return RunCodeProcessorOnFile(fFile, false, true)
end function

RunCodeProcessorOnString = function(sCode, bShorten, bPreprocess)
	cpCodeProcessor = new CodeProcessor
	cpCodeProcessor.Init(sCode, bShorten, bPreprocess)
	return cpCodeProcessor.Generate()
end function

ShortenCodeString = function(sCode)
	return RunCodeProcessorOnString(sCode, true, false)
end function

ProcessCodeString = function(sCode)
	return RunCodeProcessorOnString(sCode, false, true)
end function

// Configuration Class             : Used to manage all types of configuration
// Configuration Class Extra Notes : I based this configuration structure off of a
//									 common Linux text configuration system ( # = comment , variableName = sVariableValue )

// sFilePath   = String File Path
// fReference  = File Object to file passed
// sLoadedData = String Text in the File

Configuration = {"fReference" : "", "sFilePath" : "", "mLoadedData" : {}, "bWrite" : false, "sAppend" : "", "lNewSettings" : []}

// Initiliazes the object with the file data needed
Configuration.Init = function(sFilePath)
	// Get the configuration file
	self.fReference = GetFile(sFilePath)

	// Checking if the file exists and has read and write permissions
	if not self.fReference then exit("Error: " + sFilePath + " does not exist") end if
	if not PermissionCheck(self.fReference, "rw") then exit("Error: " + sFilePath + " does not have read or write permissions") end if
	self.sFilePath = sFilePath
	self.Parse()
end function

// Initiliazes a "pure" object with nothing in it besides the desired file path
Configuration.New = function(sFilePath)
	self.sFilePath = sFilePath
	self.fReference = GetFile(sFilePath)

	if not self.fReference then
		sParentPath = parent_path( sFilePath )

		if sParentPath == sFilePath then
			sParentPath = current_path
		end if

		cComputer.touch(sParentPath, path_file(sFilePath))
		self.fReference = GetFile(sFilePath)
	end if
end function

// Parses the file data
Configuration.Parse = function()
    mFinalData = {}

    // Loops through everyline in the file
    for sLine in self.fReference.get_content.split(char(10))
        sTrimedLine = sLine.trim

        // Checks if the line is a whitespace or comment
        if sTrimedLine == "" then continue end if
        if sTrimedLine[0] == "#" then continue end if

        // Start getting line data
        lData = SplitUntil(sTrimedLine, "=", 2)
        sName = lData[0].trim
        sData = lData[1]

        // Push the data
        mFinalData[sName] = sData
    end for

    self.mLoadedData = mFinalData
end function

// Checks if the configuration has a setting
Configuration.HasSetting = function(sSettingName)
	if self.mLoadedData.hasIndex(sSettingName) then return true end if
	return false
end function

// Returns the value of the setting
Configuration.GetSetting = function(sSettingName)
	return self.mLoadedData[sSettingName]
end function

// Sets the value of the setting
Configuration.SetSetting = function(sSettingName, oValue)
	if not self.mLoadedData[sSettingName] then
		self.lNewSettings.push(sSettingName)
	end if
	self.mLoadedData[sSettingName] = oValue
end function

// Deletes the currently loaded Configuration Data
// aka, setting mLoadedData to an empty map
// and setting the sText to an empty string
Configuration.Flush = function()
	self.mLoadedData = {}
end function

// Gets the mLoadedData so basically it returns the raw map
Configuration.GetData = function()
	return self.mLoadedData
end function

// Writes to the file object
Configuration.WriteFile = function(sString)
	self.fReference.set_content(sString)
end function

// Saves the Configuration
Configuration.Save = function()
	sFinalConfig = ""

	// Loops through everyline in the file
	for sLine in self.fReference.get_content.split(char(10))
		sTrimedLine = sLine.trim

		// Checks if the line is a whitespace or comment
		if sTrimedLine == "" then
			sFinalConfig = sFinalConfig + char(10)
			continue
		end if
		if sTrimedLine.len <= 0 then
			sFinalConfig = sFinalConfig + char(10)
			continue
		end if
		if sTrimedLine[0] == "#" then
			sFinalConfig = sFinalConfig + sLine + char(10)
			continue
		end if
		if not StringContains(sTrimedLine, "=") then sFinalConfig = sFinalConfig + sTrimedLine continue end if

		// Start getting line data
		lData = SplitUntil(sTrimedLine, "=", 2)
		sName = lData[0].trim
		
		// Change the data
		sFinalConfig = sFinalConfig + sName + "=" + self.mLoadedData[sName] + char(10)
	end for

	self.fReference.set_content(slice(sFinalConfig, 0, sFinalConfig.len - 1))
end function

// Writing functions
// Starts setting up the writing
// bFlush just checks if you want to flush the commands
Configuration.StartWrite = function(bFlush)
	if bFlush then self.Flush() end if

	self.bWrite = true
end function

Configuration.WriteLine = function()
	self.sAppend = self.sAppend + char(10)
end function

Configuration.Write = function(sSettingName, sValue)
	self.sAppend = self.sAppend + sSettingName + "=" + sValue + char(10)
end function

Configuration.WriteComment = function(sComment)
	self.sAppend = self.sAppend + "# " + sComment + char(10)
end function

Configuration.EndWrite = function()
	self.bWrite = false
	self.WriteFile(self.sAppend)
	self.sAppend = ""
	self.Parse()
end function

// ListFile Class : String list class, which can be converted into a file
ListFile = {"lConf":[], "fFile":"", "sPath":""}

ListFile.Init = function(fFile)
	self.fFile = fFile
	self.sPath = fFile.path
	self.Parse()
end function

ListFile.Create = function(sPath)
	self.sPath = sPath
	cComputer.touch(parent_path(sPath), path_file(sPath))
	self.fFile = GetFile(sPath)
end function

ListFile.GetList = function()
	return self.lConf
end function

ListFile.GetFile = function()
	return self.fFile
end function

ListFile.Parse = function()
	self.lConf = []
	for sLine in self.fFile.get_content.split(char(10))
		if sLine.trim() == "" then
			continue
		end if
		self.lConf.push(sLine)
	end for
end function

ListFile.Add = function(sString)
	self.lConf.push(sString)
end function

ListFile.Pop = function()
	return self.lConf.pop()
end function

ListFile.Pull = function()
	return self.lConf.pull()
end function

ListFile.Remove = function(i)
	return self.lConf.remove(i)
end function

ListFile.ToString = function()
	return self.lConf.join(char(10))
end function

ListFile.Save = function()
	self.fFile.set_content(self.ToString())
end function

// AssertiveConfWriter Class : A Class used for writing configs with "assert" statements
AssertiveConfWriter = {"confs":[]}

AssertiveConfWriter.AssertConfiguration = function(sPath)
	cfgConf = new Configuration

	if GetFile( sPath ) then
		cfgConf.Init(FixPath( sPath ))
	else
		cfgConf.New(sPath)
	end if

	return cfgConf
end function

AssertiveConfWriter.__get_latest_conf = function()
	return AssertiveConfWriter.confs[AssertiveConfWriter.confs.len - 1]
end function

AssertiveConfWriter.PushConf = function(cfgConf)
	cfgConf.StartWrite( false )
	AssertiveConfWriter.confs.push(cfgConf)
end function

AssertiveConfWriter.PopConf = function()
	cfgConf = AssertiveConfWriter.confs.pop()
	cfgConf.EndWrite()
	return cfgConf
end function

AssertiveConfWriter.WriteLine = function()
	AssertiveConfWriter.__get_latest_conf().WriteLine()
end function

AssertiveConfWriter.Write = function(sSettingName, sValue)
	cfgConf = AssertiveConfWriter.__get_latest_conf()

	if cfgConf.HasSetting(sSettingName) then
		cfgConf.sAppend = cfgConf.sAppend + sSettingName + "=" + cfgConf.GetSetting(sSettingName) + char(10)
		return null
	end if

	cfgConf.Write(sSettingName, sValue)
end function

AssertiveConfWriter.WriteComment = function(sComment)
	AssertiveConfWriter.__get_latest_conf().WriteComment(sComment)
end function

Timer = {time:0}

Timer.Start = function()
	Timer.time = time
end function

Timer.Stop = function()
	nSecs = time - Timer.time
	Timer.time = 0
	return nSecs
end function

Timer.StopMilli = function()
	nSecs = (time - Timer.time) * 1000
	Timer.time = 0
	return nSecs
end function

// Command Class             : Command class used as a base class for all commands
// Command Class Extra Notes : Just a simple abstraction

// sName   = String Name of the Commmand
Command = {"sName":"", "sDesc":"", "lArgs":[]}

Command.__init = function(sName, sDesc, lArgs)
	self.sName = sName
	self.sDesc = sDesc.replace(" ", cSpace)
	self.lArgs = lArgs
end function

Command.Init = function()
	Exception("Unimplemented Command.Init: " + self.sName)
end function

Command.Execute = function(Args)
	Exception("Unimplemented Command.Execute: " + self.sName)
end function

// BinTools Class : Used to create a programatic form of binaries in the actual system
BinTools = {}

BinTools.CreateBinaryTool = function(sPath)
	if not GetFile(sPath) then
		if parent_path(sPath) == sPath then
			orig_sPath = sPath
			sPath = "/bin/" + sPath
			if not GetFile(sPath) then
				sPath = current_path + orig_sPath
				if not GetFile(sPath) then
					Exception("BinTools: attempted to create a bintool " + sPath)
				end if
			end if
		else
			Exception("BinTools: attempted to create a bintool " + sPath)
		end if
	end if

	fBinary = GetFile(sPath)
	mBinTool = {"sPath":sPath, "fBinary":fBinary}

	mBinTool.Launch = function(sArgs)
		sShell.launch(self.sPath, sArgs)
	end function

	BinTools[path_file(sPath)] = mBinTool
	return mBinTool
end function

BinTools.CreateBinSet = function(sPath)
	fFolder = GetFile(sPath)
	if not fFolder then
		Exception("BinTools: attempted to create a BinSet with " + sPath)
	end if

	for fBin in fFolder.get_files
		if fBin.is_binary then
			BinTools.CreateBinaryTool(fBin.path)
		end if
	end for
end function

BinTools.CreateBinSet("/bin")

// BuildSystem Class			 : Used for the build system, aka the project's compiling
// BuildSystem Class Extra Notes : More Robusts
BuildSystem = {"cfgBuild":"", "sPath":""}

BuildSystem.Init = function(sProjectPath)
	self.sPath = sProjectPath

	if GetFile( sProjectPath + "/build.conf" ) then
		self.Parse()
	end if
end function

// # CHECK_SRCS is used for checking if
// # the libraries and the main.src for compiler
// # errors
// CHECK_SRCS=TRUE
// # GEN_LOG generates a log
// # in the obj_location
// GEN_LOG=FALSE
// # if you want to use the import_code
// # function then set it to TRUE or do you want to slap each
// # library ontop of each other keep it FALSE. (Usefull for bypassing character limit)
// IMPORT=FALSE
// # Makes the compiled file shorter by removing indents
// # and comments and empty lines
// SHORTEN_COMPILED_FILE=TRUE
BuildSystem.GenerateConfiguration = function()
	self.cfgBuild = AssertiveConfWriter.AssertConfiguration( self.sPath + "/build.conf" )

	AssertiveConfWriter.PushConf( self.cfgBuild )

	AssertiveConfWriter.WriteComment("CHECK_SRCS is used for checking if")
	AssertiveConfWriter.WriteComment("the libraries and the main.src for compiler")
	AssertiveConfWriter.WriteComment("errors")
	AssertiveConfWriter.Write("CHECK_SRCS", "TRUE")

	AssertiveConfWriter.WriteComment("GEN_LOG generates a log")
	AssertiveConfWriter.WriteComment("in the obj_location")
	AssertiveConfWriter.Write("GEN_LOG", "FALSE")

	AssertiveConfWriter.WriteComment("if you want to use the import_code")
	AssertiveConfWriter.WriteComment("function then set it to TRUE or do you want to slap each")
	AssertiveConfWriter.WriteComment("library ontop of each other keep it FALSE. (Usefull for bypassing character limit)")
	AssertiveConfWriter.Write("IMPORT", "FALSE")

	AssertiveConfWriter.WriteComment("Makes the compiled file shorter by removing indents")
	AssertiveConfWriter.WriteComment("and comments and empty lines")
	AssertiveConfWriter.Write("SHORTEN_COMPILED_FILE", "TRUE")

	AssertiveConfWriter.PopConf()
end function

BuildSystem.Parse = function()
	if not GetFile( self.sPath + "/build.conf" ) then
		Error("Build System: couldn't find a build configuration in " + self.sPath)
	end if
	
	self.cfgBuild = AssertiveConfWriter.AssertConfiguration( self.sPath + "/build.conf" )
end function

BuildSystem.GenerateLibraryBuildList = function()
	lfBuild = new ListFile
	lfBuild.Create( self.sPath + "/lib_build.list" )
	fLib = GetFile( GetProjectConfigurationFromPath(self.sPath).GetSetting("lib_location") )

	for fFile in fLib.get_files
		lfBuild.Add(fFile.name)
	end for

	lfBuild.Save()
end function

BuildSystem.GetLibraryBuildList = function()
	if GetFile( self.sPath + "/lib_build.list" ) then
		lfBuild = new ListFile
		lfBuild.Init( self.sPath + "/lib_build.list" )

		for sLib in GetFile(GetProjectConfigurationFromPath(self.sPath).GetSetting("lib_location")).get_files
			if not lfBuild.GetList().indexOf(sLib) then
				lfBuild.Add(sLib)
			end if
		end for

		return lfBuild.GetList()
	else
		lLibs = []

		for sLib in GetFile(GetProjectConfigurationFromPath(self.sPath).GetSetting("lib_location")).get_files
			lLibs.push(sLib)
		end for

		return lLibs
	end if
end function

BuildSystem.CheckSources = function()
	cfgProjectConf = GetProjectConfigurationFromPath(self.sPath)
	fObjChecks = GetFile(cfgProjectConf.GetSetting("obj_location") + "/checks")
	fObjPreprocess = GetFile(cfgProjectConf.GetSetting("obj_location") + "/preprocess")

	ERROR_COLOR = cfgColorSettings.GetSetting("ERROR_COLOR")
	SUCCESS_COLOR = cfgColorSettings.GetSetting("SUCCESS_COLOR")

	bSuccess = true

	for fLib in GetFile(cfgProjectConf.GetSetting("lib_location")).get_files
		cComputer.touch(fObjPreprocess.path, fLib.name)
		GetFile(fObjPreprocess.path + "/" + fLib.name).set_content(ProcessCodeFile(fLib))
		sResult = sShell.build(fObjPreprocess.path + "/" + fLib.name, fObjChecks.path)
		//sResult = sShell.build(fLib.path, fObjChecks.path)

		if sResult.len == 0 then
			print("    Compiling " + fLib.name + "... " + ColorCast(SUCCESS_COLOR, "compiles"))
			BuildSystem.Log(fLib.path + " compiles.")
		else
			print("    Compiling " + fLib.name + "... " + ColorCast(ERROR_COLOR, sResult))
			BuildSystem.Log(fLib.path + " doesn't compile. Reason: " + sResult)
			bSuccess = false
		end if
	end for
	cComputer.touch(fObjPreprocess.path, "Main.src")
	GetFile(fObjPreprocess.path + "/Main.src").set_content(ProcessCodeFile(GetFile(self.sPath + "/Main.src")))
	sResult = sShell.build(fObjPreprocess.path + "/Main.src", fObjChecks.path)
	//sResult = sShell.build(self.sPath + "/Main.src", fObjChecks.path)

	if sResult.len == 0 then
		print("    Compiling Main.src... " + ColorCast(SUCCESS_COLOR, "compiles"))
		BuildSystem.Log(self.sPath + "/Main.src compiles.")
	else
		print("    Compiling Main.src... " + ColorCast(ERROR_COLOR, sResult))
		BuildSystem.Log(sPath + "/Main.src doesn't compile. Reason: " + sResult)
		bSuccess = false
	end if

	return bSuccess
end function

BuildSystem.GenerateSourceHashes = function()
	lfHashes = new ListFile
	cfgProjectConf = GetProjectConfigurationFromPath(self.sPath)
	lfHashes.Create( cfgProjectConf.GetSetting("obj_location") + "/hash/" + cfgProjectConf.GetSetting("proj_version") + ".hashes.list" )

	lfHashes.Add("Main.src:" + HashFile(GetFile(self.sPath + "/Main.src")))
	BuildSystem.Log("Generated " + self.sPath + "/Main.src hash.")

	fLib = GetFile( cfgProjectConf.GetSetting("lib_location") )

	for fFile in fLib.get_files
		lfHashes.Add(fFile.name + ":" + HashFile(fFile))
		BuildSystem.Log("Generated " + fFile.path + " hash.")
	end for

	lfHashes.Save()
end function

BuildSystem.Log = function(sMessage)
	if self.cfgBuild.GetSetting("GEN_LOG").upper == "TRUE" then
		cfgProjectConf = GetProjectConfigurationFromPath(self.sPath)
		fLogFile = GetFile(cfgProjectConf.GetSetting("obj_location") + "/build.log")
		if not fLogFile then
			cComputer.touch(cfgProjectConf.GetSetting("obj_location"), "build.log")
			fLogFile = GetFile(cfgProjectConf.GetSetting("obj_location") + "/build.log")
		end if

		fLogFile.set_content(fLogFile.get_content + current_date + " - " + sMessage + char(10))
	end if
end function

BuildSystem.Build = function()
	Timer.Start()
	print("\nRunning 'project-build' on " + self.sPath + "...")
	cfgProjectConf = GetProjectConfigurationFromPath(self.sPath)

	if self.cfgBuild.GetSetting("CHECK_SRCS").upper == "TRUE" then
		print("  Checking source files...")
		if not BuildSystem.CheckSources() then
			Error("  Source check failed. Not continueing.")
		end if
	end if

	print("  Generating hashes...")
	BuildSystem.GenerateSourceHashes()

	// Generate final source
	if self.cfgBuild.GetSetting("IMPORT").upper == "TRUE" then
		lImports = []

		//if self.cfgBuild.GetSetting("SHORTEN_COMPILED_FILE").upper == "TRUE" then
		//	print("  Creating import files...")
		//	fObjectSrc = GetFile(cfgProjectConf.GetSetting("obj_location") + "/src")
		//	for fFile in GetFile(cfgProjectConf.GetSetting("lib_location")).get_files
		//		print("    Shortening " + fFile.name + "...")
		//		cComputer.touch(fObjectSrc.path, fFile.name)
		//		GetFile(fObjectSrc.path + "/" + fFile.name).set_content(ShortenCodeString(fFile.get_content))
		//		lImports.push(fObjectSrc.path + "/" + fFile.name)
		//	end for
		//else
		//	for fFile in GetFile(cfgProjectConf.GetSetting("lib_location")).get_files
		//		lImports.push(fFile.path)
		//	end for
		//end if

		print("  Creating import files...")
		bShouldShorten = self.cfgBuild.GetSetting("SHORTEN_COMPILED_FILE").upper == "TRUE"
		fObjectSrc = GetFile(cfgProjectConf.GetSetting("obj_location") + "/src")
		for fFile in GetFile(cfgProjectConf.GetSetting("lib_location")).get_files
			print("    Processing " + fFile.name + "...")
			cComputer.touch(fObjectSrc.path, fFile.name)
			GetFile(fObjectSrc.path + "/" + fFile.name).set_content(RunCodeProcessorOnFile(fFile, bShouldShorten, true))
			lImports.push(fObjectSrc.path + "/" + fFile.name)
		end for
		
		if GetFile(self.sPath + "/lib_build.list") then
			lfLibBuild = new ListFile
			lfLibBuild.Init( GetFile(self.sPath + "/lib_build.list") )
			lImports = []
			sBasePath = cfgProjectConf.GetSetting("obj_location") + "/src"

			//if self.cfgBuild.GetSetting("SHORTEN_COMPILED_FILE").upper == "TRUE" then
			//	sBasePath = cfgProjectConf.GetSetting("obj_location") + "/src"
			//else
			//	sBasePath = cfgProjectConf.GetSetting("lib_location")
			//end if

			for sName in lfLibBuild.GetList()
				lImports.push(sBasePath + "/" + sName)
			end for
		end if

		print("  Generating final source...")
		sMainSrc = GetFile(self.sPath + "/Main.src").get_content
		sFinalSrc = ""

		for sImport in lImports
			impc = "import_code"
			sFinalSrc = sFinalSrc + impc + "(""" + sImport + """)" + char(10)
		end for

		//sFinalSrc = RunCodeProcessorOnString(sFinalSrc + sMainSrc, bShouldShorten, true)
		//cComputer.touch(cfgProjectConf.GetSetting("obj_location") + "/src", cfgProjectConf.GetSetting("proj_name") + ".compiled.src")
		//GetFile(cfgProjectConf.GetSetting("obj_location") + "/src/" + cfgProjectConf.GetSetting("proj_name") + ".compiled.src").set_content(sFinalSrc)
		cComputer.touch(cfgProjectConf.GetSetting("obj_location") + "/src", cfgProjectConf.GetSetting("proj_name") + ".compiled.src")
		fFinalFile = GetFile(cfgProjectConf.GetSetting("obj_location") + "/src/" + cfgProjectConf.GetSetting("proj_name") + ".compiled.src")
		fFinalFile.set_content(sFinalSrc + sMainSrc)
		fFinalFile.set_content(RunCodeProcessorOnFile(fFinalFile, bShouldShorten, true))
	else
		print("  Generating final source...")
		sMainSrc = GetFile(self.sPath + "/Main.src").get_content
		sFinalSrc = ""

		if GetFile(self.sPath + "/lib_build.list") then
			lfLibBuild = new ListFile
			lfLibBuild.Init( GetFile(self.sPath + "/lib_build.list") )

			for sName in lfLibBuild.GetList()
				sFinalSrc = sFinalSrc + GetFile(cfgProjectConf.GetSetting("lib_location") + "/" + sName).get_content + char(10)
			end for
		else
			for fFile in GetFile(cfgProjectConf.GetSetting("lib_location")).get_files
				sFinalSrc = sFinalSrc + fFile.get_content + char(10)
			end for
		end if

		//sFinalSrc = sFinalSrc + sMainSrc

		//if self.cfgBuild.GetSetting("SHORTEN_COMPILED_FILE").upper == "TRUE" then
		//	sFinalSrc = RunCodeProcessorOnString(sFinalSrc, true, true)
		//else
		//	sFinalSrc = ProcessCodeString(sFinalSrc)
		//end if

		//cComputer.touch(cfgProjectConf.GetSetting("obj_location") + "/src", cfgProjectConf.GetSetting("proj_name") + ".compiled.src")
		//GetFile(cfgProjectConf.GetSetting("obj_location") + "/src/" + cfgProjectConf.GetSetting("proj_name") + ".compiled.src").set_content(sFinalSrc)
		cComputer.touch(cfgProjectConf.GetSetting("obj_location") + "/src", cfgProjectConf.GetSetting("proj_name") + ".compiled.src")
		fFinalFile = GetFile(cfgProjectConf.GetSetting("obj_location") + "/src/" + cfgProjectConf.GetSetting("proj_name") + ".compiled.src")
		fFinalFile.set_content(sFinalSrc + sMainSrc)

		if self.cfgBuild.GetSetting("SHORTEN_COMPILED_FILE").upper == "TRUE" then
			fFinalFile.set_content(RunCodeProcessorOnFile(fFinalFile, true, true))
		else
			fFinalFile.set_content(ProcessCodeFile(fFinalFile))
		end if
	end if

	cComputer.create_folder(cfgProjectConf.GetSetting("bin_location"), cfgProjectConf.GetSetting("proj_version"))
	sResult = sShell.build(cfgProjectConf.GetSetting("obj_location") + "/src/" + cfgProjectConf.GetSetting("proj_name") + ".compiled.src", cfgProjectConf.GetSetting("bin_location") + "/" + cfgProjectConf.GetSetting("proj_version"))

	if sResult.len == 0 then
		print("  Building final source... " + ColorCast(cfgColorSettings.GetSetting("SUCCESS_COLOR"), "Success!"))
		if GetFile(cfgProjectConf.GetSetting("bin_location") + "/" + cfgProjectConf.GetSetting("proj_version") + "/" + cfgProjectConf.GetSetting("proj_name")) then
			GetFile(cfgProjectConf.GetSetting("bin_location") + "/" + cfgProjectConf.GetSetting("proj_version") + "/" + cfgProjectConf.GetSetting("proj_name")).delete()
		end if
		GetFile(cfgProjectConf.GetSetting("bin_location") + "/" + cfgProjectConf.GetSetting("proj_version") + "/" + cfgProjectConf.GetSetting("proj_name") + ".compiled").rename(cfgProjectConf.GetSetting("proj_name"))
		BuildSystem.Log("Final source compiles")
	else
		print("  Building final source... " + ColorCast(cfgColorSettings.GetSetting("ERROR_COLOR"), sResult))
		BuildSystem.Log("Final source fails to compile. Reason: " + sResult)
		Error("Build failed in " + Timer.StopMilli() + " ms for " + self.sPath + ".")
	end if
	
	print("  Build completed in " + Timer.StopMilli() + " ms for " + self.sPath + ".\n")
	print("Build succeeded.\n")
end function

// StringBuilder Class			   : Used to create strings easier
// StringBuilder Class Extra Notes : Sort of like a simplied version of C#'s StringBuilder. Should be used for columns
StringBuilder = {"sString":""}

StringBuilder.Append = function(sString)
	self.sString = self.sString + sString
end function

StringBuilder.AppendLine = function(sString)
	self.sString = self.sString + sString + char(10)
end function

StringBuilder.ToString = function()
	return self.sString
end function

// ExportSystem Class : Used to create a sort of "manifest" file for projects to save
ExportSystem = {"sPath":"", "cfgProjectConf":""}

ExportSystem.Init = function(sPath)
	self.sPath = sPath
	self.cfgProjectConf = GetProjectConfigurationFromPath(sPath)
end function

ExportSystem.Export = function(bSave)
	print("\nRunning 'generate-project-manifest' on " + self.sPath + "...")
	print("  Creating manifest-file " + self.sPath + "/" + self.cfgProjectConf.GetSetting("proj_name") + ".sef...")
	print("  Generating manifest-script...")
	esgManifest = new ExportSystem.Generator
	esgManifest.Init( self.sPath + "/" + self.cfgProjectConf.GetSetting("proj_name") + ".sef" )
	
	esgManifest.CreateSegment("CONFIGURATION SETUP:" + self.cfgProjectConf.GetSetting("proj_name"))
	esgManifest.AddCommand("print", ["Generating project..."])
	esgManifest.AddCommand("create_base", [])
	esgManifest.AddCommand("create_file", ["/usr/tmp.conf"])
	esgManifest.AddCommand("edit_file", ["/usr/tmp.conf"])
	esgManifest.AddFileString(self.cfgProjectConf.fReference.get_content)
	esgManifest.AddCommand("port_project_conf", [])
	esgManifest.AddCommand("create_project_skeleton", [])
	esgManifest.EndSegment()

	esgManifest.CreateSegment("Main Loading")
	esgManifest.AddCommand("print", ["Generating Main..."])
	esgManifest.AddCommand("create_file_in_proj", ["Main.src"])
	esgManifest.AddCommand("edit_file_in_proj", ["Main.src"])
	esgManifest.AddFileString(GetFile(current_path + "/Main.src").get_content)
	esgManifest.EndSegment()

	esgManifest.CreateSegment("Library Loading")
	for fLib in GetFile(self.cfgProjectConf.GetSetting("lib_location")).get_files
		esgManifest.AddCommand("print", ["Generating Library " + fLib.name + "..."])
		esgManifest.AddCommand("create_file_in_proj_lib", [fLib.name])
		esgManifest.AddCommand("edit_file_in_proj_lib", [fLib.name])
		esgManifest.AddFileString(fLib.get_content)
	end for
	esgManifest.EndSegment()

	esgManifest.CreateSegment("Other Loading")
	esgManifest.AddCommand("print", ["Generating Build Configuration..."])
	esgManifest.AddCommand("create_file_in_proj", ["build.conf"])
	esgManifest.AddCommand("edit_file_in_proj", ["build.conf"])
	esgManifest.AddFileString(GetFile(current_path + "/build.conf").get_content)

	if GetFile(current_path + "/lib_build.list") then
		esgManifest.AddCommand("print", ["Generating lbl file..."])
		esgManifest.AddCommand("create_file_in_proj", ["lib_build.list"])
		esgManifest.AddCommand("edit_file_in_proj", ["lib_build.list"])
		esgManifest.AddFileString(GetFile(current_path + "/lib_build.list").get_content)
	end if

	esgManifest.EndSegment()
	if bSave then
		esgManifest.Save()
	else
		esgManifest.fFile.delete()
	end if
	return esgManifest.lExport
end function

ExportSystem.Generator = {"fFile":"", "lExport":[], "sShell":""}

ExportSystem.Generator.Init = function(sPath)
	self.sShell = sShell
	cComputer.touch(parent_path(sPath), path_file(sPath))
	self.fFile = GetFile(sPath)
	self.lExport.push("# Synapse Export Format (SEF) {https://gitlab.com/H3xad3cimalDev/synapse}")
end function

ExportSystem.Generator.InitRemotely = function(sPath, sShell)
	self.sShell = sShell
	sShell.host_computer.touch(parent_path(sPath), path_file(sPath))
	self.fFile = sShell.host_computer.File(sPath)
end function

ExportSystem.Generator.CreateSegment = function(sSegmentName)
	self.lExport.push("@ " + sSegmentName)
end function

ExportSystem.Generator.EndSegment = function()
	self.lExport.push("%")
end function

ExportSystem.Generator.AddFileString = function(sString)
	for sLine in sString.split(char(10))
		if sLine.trim() == "" then
			self.lExport.push("    >")
			continue
		end if
		self.lExport.push("    >" + sLine)
	end for
	self.lExport.push("<")
end function

ExportSystem.Generator.AddCommand = function(sCommandName, lArgs)
	self.lExport.push("~" + sCommandName + " " + lArgs.join(" "))
end function

ExportSystem.Generator.AddComment = function(sComment)
	self.lExport.push("# " + sComment)
end function

ExportSystem.Generator.Save = function()
	self.fFile.set_content(self.lExport.join(char(10)))
end function

// In Bashur's honor
ExportSystem.Evaluate = function(sManifestCode, sTabbing, sPath)
	sLines = sManifestCode.split(char(10))
	nSkipCounter = 0
	nIndx = 0

	eLineType = {}
	eLineType.SEGMENT_START = 0
	eLineType.SEGMENT_END   = 1
	eLineType.FILE_STRING   = 2
	eLineType.FILE_END      = 3
	eLineType.COMMAND       = 4
	eLineType.COMMENT		= 5

	mStr2Type = {"@":eLineType.SEGMENT_START, "%":eLineType.SEGMENT_END, ">":eLineType.FILE_STRING, "<":eLineType.FILE_END, "~":eLineType.COMMAND, "#":eLineType.COMMENT}

	lSegments = []
	
	mLoadedData = {}
	sCurrentSegment = null
	sProjectPath = ""

	mSEFCommands = {}

	mSEFCommands.create_base = function(lArgs)
		cComputer.create_folder(sPath, sCurrentSegment.split(":")[1])
	end function

	mSEFCommands.__get_pconf = function(lArgs)
		return null
	end function

	mSEFCommands.port_project_conf = function(lArgs)
		cfgProjectConf = AssertiveConfWriter.AssertConfiguration( "/usr/tmp.conf" )
		mPConf = cfgProjectConf.mLoadedData
		sProjectPath = sPath + "/" + sCurrentSegment.split(":")[1]
		cfgPConf = AssertiveConfWriter.AssertConfiguration(sProjectPath + "/project.conf")

		// # Metadata
		// proj_name=PROJECT_NAME
		// proj_version=1.0
		cfgPConf.WriteComment( "Metadata" )
		cfgPConf.Write( "proj_name"    , mPConf["proj_name"] )
		cfgPConf.Write( "proj_version" , mPConf["proj_version"] )

		cfgPConf.WriteLine()

		// # Locations
		// bin_location=
		// lib_location=
		// obj_location=
		cfgPConf.WriteComment( "Locations" )
		cfgPConf.Write( "bin_location" , sProjectPath + "/" + path_file(mPConf["bin_location"]) )
		cfgPConf.Write( "lib_location" , sProjectPath + "/" + path_file(mPConf["lib_location"]) )
		cfgPConf.Write( "obj_location" , sProjectPath + "/" + path_file(mPConf["obj_location"]) )
		cfgPConf.EndWrite()

		mLoadedData["proj_name"]    = mPConf["proj_name"]
		mLoadedData["proj_version"] = mPConf["proj_version"]

		mLoadedData["bin_location"] = sProjectPath + "/" + path_file(mPConf["bin_location"])
		mLoadedData["lib_location"] = sProjectPath + "/" + path_file(mPConf["lib_location"])
		mLoadedData["obj_location"] = sProjectPath + "/" + path_file(mPConf["obj_location"])
		mLoadedData["path"] = sProjectPath

		GetFile("/usr/tmp.conf").delete()
	end function

	mSEFCommands.create_project_skeleton = function(lArgs)
		bin_location = mLoadedData["bin_location"]
		lib_location = mLoadedData["lib_location"]
		obj_location = mLoadedData["obj_location"]

		cComputer.create_folder(parent_path(bin_location), path_file(bin_location))
		cComputer.create_folder(parent_path(lib_location), path_file(lib_location))
		cComputer.create_folder(parent_path(obj_location), path_file(obj_location))

		cComputer.create_folder(obj_location, "hash")
		cComputer.create_folder(obj_location, "checks")
		cComputer.create_folder(obj_location, "src")
		cComputer.create_folder(obj_location, "preprocess")

		BuildSystem.Init( mLoadedData["path"] )
	end function

	mSEFCommands.create_file = function(lArgs)
		cComputer.touch(parent_path(lArgs[0]), path_file(lArgs[0]))
	end function

	mSEFCommands.create_file_in_proj = function(lArgs)
		mSEFCommands.create_file([mLoadedData["path"] + "/" + lArgs[0]])
	end function

	mSEFCommands.edit_file_in_proj = function(lArgs)
		mSEFCommands.edit_file([mLoadedData["path"] + "/" + lArgs[0]])
	end function

	mSEFCommands.create_file_in_proj_lib = function(lArgs)
		mSEFCommands.create_file([mLoadedData["lib_location"] + "/" + lArgs[0]])
	end function

	mSEFCommands.edit_file_in_proj_lib = function(lArgs)
		mSEFCommands.edit_file([mLoadedData["lib_location"] + "/" + lArgs[0]])
	end function

	mSEFCommands.edit_file = function(lArgs)
		nStartIndx = nIndx + 1
		lFileContent = []
		
		if nStartIndx > sLines.len then
			return
		end if

		for nIndx2 in range(nStartIndx, sLines.len)
			sLine = sLines[nIndx2].trim()
			if sLine.len < 1 then
				continue
			end if

			eType = mStr2Type[sLine[0]]

			if not (eType == eLineType.COMMENT or eType == eLineType.FILE_STRING or eType == eLineType.FILE_END) then
				break
			end if

			if eType == eLineType.COMMENT then
				continue
			end if

			if sLine == ">" then
				lFileContent.push("")
				continue
			end if

			sLine = sLine[1:]
			if sLine == null then continue end if

			lFileContent.push(sLine)
		end for
		
		GetFile(lArgs[0]).set_content(lFileContent.join(char(10)))
	end function

	mSEFCommands.print = function(lArgs)
		print(sTabbing + "[SEF MANIFEST] " + lArgs.join(" "))
	end function

	for nLineIndx in range(0, sLines.len - 1)
		nIndx = nLineIndx
		if not nSkipCounter == 0 then
			nSkipCounter = nSkipCounter - 1
			continue
		end if

		sLine = sLines[nLineIndx].trim()

		if sLine.len < 1 then
			continue
		end if

		eType = mStr2Type[sLine[0]]
		
		if eType == eLineType.COMMENT or eType == eLineType.FILE_STRING then
			continue
		else if eType == eLineType.SEGMENT_START then
			sSegment = sLine[1:]
			sCurrentSegment = sSegment
			lSegments.push(sSegment)
		else if eType == eLineType.SEGMENT_END then
			sCurrentSegment = null
		else if eType == eLineType.COMMAND then
			sCommand = sLine[1:]
			
			lLineData = sCommand.split(" ")
			sCommandName = lLineData.pull()
			mSEFCommands[sCommandName](lLineData)
		end if
	end for
end function

// SynxProject Class : Project on a Synx Server
CreateBasicSef = function(sProjectName, sPath, sShell)
	esgManifest = new ExportSystem.Generator
	esgManifest.InitRemotely( sPath, sShell )
	
	esgManifest.CreateSegment("CONFIGURATION SETUP:" + sProjectName)
	esgManifest.AddCommand("create_base", [])
	esgManifest.AddCommand("create_file", ["/usr/tmp.conf"])
	esgManifest.AddCommand("edit_file", ["/usr/tmp.conf"])
	esgManifest.AddFileString("# Metadata" + char(10) + "proj_name=" + sProjectName + char(10) + "proj_version=1.0" + char(10) + char(10) + "# Locations" + char(10) + "bin_location=/bin" + char(10) + "lib_location=/lib" + char(10) + "obj_location=/obj")
	esgManifest.AddCommand("port_project_conf", [])
	esgManifest.AddCommand("create_project_skeleton", [])
	esgManifest.EndSegment()

	esgManifest.CreateSegment("Main Loading")
	esgManifest.AddCommand("create_file_in_proj", ["Main.src"])
	esgManifest.AddCommand("edit_file_in_proj", ["Main.src"])
	esgManifest.AddFileString(sDefaultMain)
	esgManifest.EndSegment()

	esgManifest.CreateSegment("Other Loading")
	esgManifest.AddCommand("create_file_in_proj", ["build.conf"])
	esgManifest.AddCommand("edit_file_in_proj", ["build.conf"])
	esgManifest.AddFileString("# CHECK_SRCS is used for checking if" + char(10) + "# the libraries and the main.src for compiler" + char(10) + "# errors" + char(10) + "CHECK_SRCS=TRUE" + char(10) + "# GEN_LOG generates a log" + char(10) + "# in the obj_location" + char(10) + "GEN_LOG=FALSE" + char(10) + "# if you want to use the import_code" + char(10) + "# function then set it to TRUE or do you want to slap each" + char(10) + "# library ontop of each other keep it FALSE. (Usefull for bypassing character limit)" + char(10) + "IMPORT=TRUE" + char(10) + "# Makes the compiled file shorter by removing indents" + char(10) + "# and comments and empty lines" + char(10) + "SHORTEN_COMPILED_FILE=TRUE")
	esgManifest.EndSegment()
	
	esgManifest.Save()

	return esgManifest.sExport
end function

SynxProject = {"sProjectName":"", "sSynxShell":"", "lBranches":[], "sPath":""}

SynxProject.Init = function(sProjectName, sSynxShell)
	self.sProjectName = sProjectName
	self.sSynxShell = sSynxShell
	self.sPath = "/synx/" + sProjectName
	self.lBranches = []

	self.RefreshBranchList()
end function

SynxProject.BranchExists = function(sBranchName)
	return self.lBranches.indexOf(sBranchName) != null
end function

SynxProject.RefreshBranchList = function()
	self.lBranches = []
	for branch in self.sSynxShell.host_computer.File(self.sPath).get_folders
		self.lBranches.push(branch.name)
	end for
end function

SynxProject.RemoveBranch = function(sBranchName)
	branch = self.sSynxShell.host_computer.File(self.sPath + "/" + sBranchName)
	if not branch then return false end if
	branch.delete()
	self.RefreshBranchList()
	return true
end function

SynxProject.CreateBranch = function(sBranchName)
	self.sSynxShell.host_computer.create_folder(self.sPath + "/", sBranchName)
	CreateBasicSef(self.sProjectName, self.sPath + "/" + sBranchName + "/proj.sef", self.sSynxShell)
end function

SynxProject.SetSef = function(sBranchName, sSef)
	self.sSynxShell.host_computer.File(self.sPath + "/" + sBranchName + "/proj.sef").set_content(sSef)
end function

SynxProject.GetSef = function(sBranchName)
	return self.sSynxShell.host_computer.File(self.sPath + "/" + sBranchName + "/proj.sef").get_content
end function

// SynxServer Class : Server for high end project managing
SynxServer = {"sSynxShell":""}

SynxServer.SetupServer = function(serverPassword)
	cComputer.create_user("synx", serverPassword)

	cComputer.create_folder("/", "synx")

	fSynxFolder = cComputer.File("/synx")

	fSynxFolder.set_owner("synx", true)
	fSynxFolder.chmod("u+wrx", true)
	fSynxFolder.chmod("g-wrx", true)
	//fSynxFolder.chmod("o-wrx", true)
end function

SynxServer.CreateProject = function(sProjectName)
	if self.ProjectExists(sProjectName) then
		return
	end if
	self.sSynxShell.host_computer.create_folder("/synx", sProjectName)
	spProject = new SynxProject
	spProject.Init( sProjectName, self.sSynxShell )
	spProject.CreateBranch("master")
	return spProject
end function

SynxServer.RemoveProject = function(sProjectName)
	if not self.ProjectExists(sProjectName) then
		return
	end if

	self.sSynxShell.host_computer.File("/synx/" + sProjectName).delete()
end function

SynxServer.ProjectExists = function(sProjectName)
	return self.sSynxShell.host_computer.File("/synx/" + sProjectName) != null
end function

SynxServer.GetProject = function(sProjectName)
	spProject = new SynxProject
	spProject.Init( sProjectName, self.sSynxShell )
	return spProject
end function

SynxServer.Connect = function(sServerIP, sServerPassword, nPort)
	sSynxShell = sShell.connect_service(sServerIP, nPort, "synx", sServerPassword, "ssh")
	if typeof(sSynxShell) == "string" then Error("Attempted to connect to Synx Server (IP: " + sServerIP + " | Port: " + nPort + "): " + sSynxShell) end if
	if not sSynxShell then Error("Attempted to connect to Synx Server (IP: " + sServerIP + " | Port: " + nPort + ")") end if
	self.sSynxShell = sSynxShell
end function

// SynxConfig Class : Local Configuration to Synx Server
SynxConfig = {"sHost":"", "nPort":"", "sPassword":"", "sCurrentBranch":"", "cfgSynxConfig":"", "sProjectName":""}

SynxConfig.Init = function(sPath)
	cfgSynxConfig = new Configuration
	cfgSynxConfig.Init( sPath + "/.synx/server.conf" )
	
	self.cfgSynxConfig = cfgSynxConfig
	self.sHost = cfgSynxConfig.GetSetting( "HOST" )
	self.nPort = cfgSynxConfig.GetSetting( "PORT" ).to_int
	self.sPassword = cfgSynxConfig.GetSetting( "PASSWORD" )
	self.sProjectName = cfgSynxConfig.GetSetting( "PROJECT" )
	self.sCurrentBranch = cfgSynxConfig.GetSetting( "BRANCH" )
end function

SynxConfig.New = function(sPath, sHost, nPort, sPassword, sProjectName)
	cComputer.create_folder(sPath, ".synx")
	cfgSynxConfig = AssertiveConfWriter.AssertConfiguration(sPath + "/.synx/server.conf")
	AssertiveConfWriter.PushConf( cfgSynxConfig )

	AssertiveConfWriter.Write("HOST", sHost)
	AssertiveConfWriter.Write("PORT", nPort)
	AssertiveConfWriter.Write("PASSWORD", sPassword)
	AssertiveConfWriter.Write("PROJECT", sProjectName)
	AssertiveConfWriter.Write("BRANCH", "master")

	AssertiveConfWriter.PopConf()

	self.cfgSynxConfig = cfgSynxConfig
	self.sHost = sHost
	self.nPort = nPort
	self.sPassword = sPassword
	self.sProjectName = sProjectName
	self.sCurrentBranch = "master"
end function

SynxConfig.Connect = function()
	ssSynxServer = new SynxServer
	ssSynxServer.Connect(self.sHost, self.sPassword, self.nPort)
	return ssSynxServer
end function

// GSToken Class : Syntax Token for GreyScript
eTokenType = {}
eTokenType.WhitespaceToken   = 0 //
eTokenType.OperatorToken     = 1 // +, -, /, *, %, ^, @, <, >, =, (, ), [, ], {, }, ., ,
eTokenType.KeywordToken      = 2 // if, then, else, end, function, while, true, false, for, return, not, in
eTokenType.BuiltinToken      = 3
eTokenType.IdentifierToken   = 4
eTokenType.NumberToken       = 5
eTokenType.CommentToken      = 6 // //
eTokenType.StringToken       = 7
eTokenType.FunctionCallToken = 8
eTokenType.UnkownToken       = 9
eTokenType.PreprocessorToken = 10
eTokenType.TabToken			 = 11
eTokenType.EndOfLineToken    = 12
eTokenType.EndOfFileToken    = 13

TokenType2String = function(eType)
	for t in eTokenType
		if t.value == eType then
			return t.key
		end if
	end for
end function

lKeywords = ["if", "then", "else", "end", "function", "while", "true", "false", "for", "return", "not", "in", "self", "new"]
lOperators = ["+", "-", "/", "*", "%", "^", "@", "<", ">", "=", "(", ")", "[", "]", "{", "}", ".", ",", "!", ":", ";"]
lIdentifiers = String2CharArray("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890_")
lBuiltins = ["get_ports", "File", "create_folder", "is_network_active", "local_ip", "public_ip", "touch", "show_procs", "network_devices", "change_password", "create_user", "delete_user", "create_group", "delete_group", "groups", "close_program", "wifi_networks", "connect_wifi", "connect_ethernet", "network_gateway", "active_net_card", "typeof", "get_router", "get_switch", "nslookup", "print", "clear_screen", "active_user", "home_dir", "get_shell", "mail_login", "user_input", "include_lib", "exit", "user_mail_address", "user_bank_number", "whois", "wait", "command_info", "program_path", "current_path", "launch_path", "format_columns", "current_date", "is_lan_ip", "parent_path", "is_valid_ip", "bitwise", "import_code", "ping_port", "port_info", "used_ports", "device_ports", "devices_lan_ip", "essid_name", "bssid_name", "kernel_version", "firewall_rules", "copy", "move", "rename", "path", "parent", "name", "get_content", "set_content", "is_binary", "allow_import", "has_permission", "delete", "get_folders", "get_files", "chmod", "permissions", "owner", "set_owner", "group", "set_group", "size", "get_lan_ip", "is_closed", "port_number", "connect_service", "start_terminal", "ping", "scp", "put", "build", "launch", "host_computer", "decipher", "aircrack", "aireplay", "airmon", "smtp_user_list", "show", "search", "update", "add_repo", "del_repo", "install", "check_upgrade", "load", "net_use", "scan", "scan_address", "sniffer", "rshell_client", "rshell_server", "overflow", "lib_name", "version", "dump_lib", "fetch", "read", "send"]

GSToken = {"sText":"", "nPosition":0, "eType":""}

GSToken.Init = function(sText, nPosition, eType)
	self.sText = sText
	self.nPosition = nPosition
	self.eType = eType
end function

// GSLexer Class : Lexer for GreyScript... Mostly for SyntaxHighlighting
GSLexer = {"sCode":"", "nPosition":0}

GSLexer.Init = function(sCode)
	self.sCode = sCode
	self.nPosition = 0
end function

GSLexer.Next = function()
	self.nPosition = self.nPosition + 1
end function

GSLexer.GetCurrent = function()
	if self.nPosition >= self.sCode.len then
		return char(0)
	end if

	return self.sCode[self.nPosition]
end function

GSLexer.CreateToken = function(sText, nPosition, eType)
	t = new GSToken
	t.Init(sText, nPosition, eType)
	return t
end function

GSLexer.Lookahead = function(nOffset)
	nCurrentPosition = self.nPosition
	self.nPosition = self.nPosition + nOffset
	tToken = self.Lex()
	self.nPosition = nCurrentPosition
	return tToken
end function

GSLexer.IsAtEnd = function()
	return self.nPosition >= self.sCode.len
end function

GSLexer.Lex = function()
	if self.IsAtEnd() then
		return self.CreateToken(char(0), self.nPosition, eTokenType.EndOfFileToken)
	end if

	sLookahead = char(0)
	if not (self.nPosition + 1 >= self.sCode.len) then
		sLookahead = self.sCode[self.nPosition + 1]
	end if

	if IsNumber(self.GetCurrent()) then
		nStart = self.nPosition

		while true
			if (not IsNumber(self.GetCurrent()) and self.GetCurrent() != ".") or self.IsAtEnd() then break end if
			self.Next()
		end while

		sText = self.sCode[nStart:self.nPosition]
		return self.CreateToken(sText, nStart, eTokenType.NumberToken)
	end if

	if self.GetCurrent() == " " then
		nStart = self.nPosition

		while true
			if not self.GetCurrent() == " " or self.IsAtEnd() then break end if
			self.Next()
		end while

		sText = self.sCode[nStart:self.nPosition]
		return self.CreateToken(sText, nStart, eTokenType.WhitespaceToken)
	end if

	if self.GetCurrent() == """" then
		nStart = self.nPosition

		while true
			self.Next()
			if self.GetCurrent() == """" or self.IsAtEnd() then break end if
		end while

		self.Next()

		sText = self.sCode[nStart:self.nPosition]
		return self.CreateToken(sText, nStart, eTokenType.StringToken)
	end if

	if self.GetCurrent() == "/" and sLookahead == "/" then
		nStart = self.nPosition

		while true
			if not self.GetCurrent() != char(10) or self.IsAtEnd() then break end if
			self.Next()
		end while

		sText = self.sCode[nStart:self.nPosition]
		return self.CreateToken(sText, nStart, eTokenType.CommentToken)
	end if

	if lOperators.indexOf(self.GetCurrent()) != null then
		nStart = self.nPosition

		while true
			if not lOperators.indexOf(self.GetCurrent()) != null or self.IsAtEnd() then break end if
			self.Next()
		end while
		sText = self.sCode[nStart:self.nPosition]

		return self.CreateToken(sText, nStart, eTokenType.OperatorToken)
	end if

	if self.GetCurrent() == "#" then
		nStart = self.nPosition
		self.Next()
		while true
			if not lIdentifiers.indexOf(self.GetCurrent()) != null or self.IsAtEnd() then break end if
			self.Next()
		end while

		sText = self.sCode[nStart:self.nPosition]

		return self.CreateToken(sText, nStart, eTokenType.PreprocessorToken)
	end if

	if lIdentifiers.indexOf(self.GetCurrent()) != null then
		nStart = self.nPosition

		while true
			if not lIdentifiers.indexOf(self.GetCurrent()) != null or self.IsAtEnd() then break end if
			self.Next()
		end while

		sText = self.sCode[nStart:self.nPosition]

		if lBuiltins.indexOf(sText) != null then
			return self.CreateToken(sText, nStart, eTokenType.BuiltinToken)
		end if
		
		if lKeywords.indexOf(sText) != null then
			return self.CreateToken(sText, nStart, eTokenType.KeywordToken)
		end if

		if not (self.nPosition + 1 >= self.sCode.len) then
			tLookahead = self.Lookahead(0)
			if tLookahead.eType == eTokenType.WhitespaceToken then
				tLookahead = self.Lookahead(tLookahead.sText.len)
				if tLookahead.eType == eTokenType.OperatorToken and tLookahead.sText[0] == "(" then
					return self.CreateToken(sText, nStart, eTokenType.FunctionCallToken)
				end if
			else if tLookahead.eType == eTokenType.OperatorToken and tLookahead.sText[0] == "(" then
				return self.CreateToken(sText, nStart, eTokenType.FunctionCallToken)
			end if
		end if

		return self.CreateToken(sText, nStart, eTokenType.IdentifierToken)
	end if
	
	if self.GetCurrent() == char(10) then
		self.Next()
		return self.CreateToken(char(10), self.nPosition - 1, eTokenType.EndOfLineToken)
	end if

	if self.GetCurrent() == char(9) then
		self.Next()
		return self.CreateToken(char(9), self.nPosition - 1, eTokenType.TabToken)
	end if

	sUnkown = self.GetCurrent()
	nUnkown = self.nPosition
	self.Next()
	return self.CreateToken(sUnkown, nUnkown, eTokenType.UnkownToken)
end function

CodeProcessor = {"sCode":"", "bShorten":false, "bPreprocess":false, "mMacros":{}, "fFile":""}

CodeProcessor.Init = function(sCode, bShorten, bPreprocess)
	self.sCode = sCode
	self.bShorten = bShorten
	self.bPreprocess = bPreprocess
	self.mMacros = {}
	self.fFile = null
end function

CodeProcessor.InitFile = function(fFile, bShorten, bPreprocess)
	self.sCode = fFile.get_content
	self.bShorten = bShorten
	self.bPreprocess = bPreprocess
	self.mMacros = {}
	self.fFile = fFile
end function

CodeProcessor.Generate = function()
	lLexer = new GSLexer
	lLexer.Init(self.sCode)
	sProcessed = ""

	tLastToken = new GSToken
	tLastToken.eType = eTokenType.EndOfLineToken
	while true
		tToken = lLexer.Lex()

		if not tToken.eType != eTokenType.EndOfFileToken then break end if

		if tLastToken.eType == eTokenType.EndOfLineToken then
			IsAtEnd = function()
				tLookahead = lLexer.Lookahead(1) 
				if tLookahead.eType != eTokenType.EndOfFileToken then
					if tLookahead.eType == eTokenType.WhitespaceToken or tLookahead.eType == eTokenType.TabToken then
						return lLexer.Lookahead(tLookahead.sText.len).eType == eTokenType.EndOfFileToken
					end if
				else
					return true
				end if

				return false
			end function
			if tToken.eType == eTokenType.PreprocessorToken and self.bPreprocess == true then
				if tToken.sText == "#define" then
					if lLexer.Lex().eType != eTokenType.WhitespaceToken then
						Error("Expected whitespace token")
					end if
					tIdentifier = lLexer.Lex()

					if tIdentifier.eType != eTokenType.IdentifierToken then
						Error("Not a identifier for macro: " + tIdentifier.sText)
					end if

					tNext = lLexer.Lex()

					if tNext.eType == eTokenType.EndOfLineToken or tNext.eType == eTokenType.EndOfFileToken then
						self.mMacros[tIdentifier.sText] = ""
						continue
					end if
					if tNext.eType != eTokenType.WhitespaceToken then
						Error("Expected Whitespace Token or EndOfLine Token")
					end if

					lMacroTexts = []
					while true
						tMToken = lLexer.Lex()
						if tMToken.eType == eTokenType.EndOfLineToken or tMToken.eType == eTokenType.EndOfFileToken then break end if
						lMacroTexts.push(tMToken.sText)
					end while

					sMacroText = lMacroTexts.join("")
					
					// map macro
					self.mMacros[tIdentifier.sText] = sMacroText
				else if tToken.sText == "#include" then
					if lLexer.Lex().eType != eTokenType.WhitespaceToken then
						Error("Expected whitespace token")
					end if
					tString = lLexer.Lex()

					if tString.eType != eTokenType.StringToken then
						Error("Not a string for macro: " + tIdentifier.sText)
					end if

					sPath = FixPath(tString.sText.replace("""", ""))
					fInclude = GetFile(sPath)
					if not fInclude then
						Error("Could not find file: " + sPath)
					end if

					cpCodeProcessor = new CodeProcessor
					cpCodeProcessor.InitFile(fInclude, self.bShorten, self.bPreprocess)
					cpCodeProcessor.mMacros = self.mMacros
					sInclude = cpCodeProcessor.Generate()

					for mMacro in cpCodeProcessor.mMacros
						self.mMacros[mMacro.key] = mMacro.value
					end for

					if sInclude.len > 0 and sInclude[sInclude.len - 1] != char(10) and IsAtEnd() then
						sInclude = sInclude + char(10)
					end if

					sProcessed = sProcessed + sInclude
				else if tToken.sText == "#ifdef" then
					if lLexer.Lex().eType != eTokenType.WhitespaceToken then
						Error("Expected whitespace token")
					end if
					tIdentifier = lLexer.Lex()

					if tIdentifier.eType != eTokenType.IdentifierToken then
						Error("Not a identifier for ifdef: " + tIdentifier.sText)
					end if

					if not self.mMacros.hasIndex(tIdentifier.sText) then
						nCount = 0
						while true
							tMToken = lLexer.Lex()
							if (tMToken.eType == eTokenType.PreprocessorToken and tMToken.sText == "#ifndef" or tMToken.sText == "#ifdef") then
								nCount = nCount + 1
								continue
							end if
							if (tMToken.eType == eTokenType.PreprocessorToken and tMToken.sText == "#endif") then
								if nCount == 0 then
									break
								end if
								nCount = nCount - 1
							end if
							if tMToken.eType == eTokenType.EndOfFileToken then break end if
						end while
						continue
					end if

					lSectionText = []
					tLast = new GSToken
					tLast.eType = eTokenType.EndOfLineToken
					nCount = 0
					while true
						tMToken = lLexer.Lex()
						if ((tLast.eType == eTokenType.EndOfLineToken and tMToken.eType == eTokenType.PreprocessorToken) and tMToken.sText == "#endif") then
							if nCount == 0 then
								lSectionText.pop()
								break
							end if
							nCount = nCount - 1
						end if
						if tMToken.eType == eTokenType.EndOfFileToken then break end if
						if (tMToken.eType == eTokenType.PreprocessorToken and tMToken.sText == "#ifdef" or tMToken.sText == "#ifndef") then
							nCount = nCount + 1
						end if
						lSectionText.push(tMToken.sText)
						tLast = tMToken
					end while
					lSectionText.pull()

					cpCodeProcessor = new CodeProcessor
					
					cpCodeProcessor.Init(lSectionText.join(""), self.bShorten, self.bPreprocess)
					cpCodeProcessor.mMacros = self.mMacros
					sSection = cpCodeProcessor.Generate()

					for mMacro in cpCodeProcessor.mMacros
						self.mMacros[mMacro.key] = mMacro.value
					end for

					if sSection.len > 0 and sSection[sSection.len - 1] != char(10) or not IsAtEnd() then
						sSection = sSection + char(10)
					end if

					sProcessed = sProcessed + sSection
				else if tToken.sText == "#ifndef" then
					if lLexer.Lex().eType != eTokenType.WhitespaceToken then
						Error("Expected whitespace token")
					end if
					tIdentifier = lLexer.Lex()

					if tIdentifier.eType != eTokenType.IdentifierToken then
						Error("Not a identifier for ifndef: " + tIdentifier.sText)
					end if

					if self.mMacros.hasIndex(tIdentifier.sText) then
						nCount = 0
						while true
							tMToken = lLexer.Lex()
							if (tMToken.eType == eTokenType.PreprocessorToken and tMToken.sText == "#ifndef" or tMToken.sText == "#ifdef") then
								nCount = nCount + 1
								continue
							end if
							if (tMToken.eType == eTokenType.PreprocessorToken and tMToken.sText == "#endif") then
								if nCount == 0 then
									break
								end if
								nCount = nCount - 1
							end if
							if tMToken.eType == eTokenType.EndOfFileToken then break end if
						end while
						continue
					end if

					lSectionText = []
					tLast = new GSToken
					tLast.eType = eTokenType.EndOfLineToken
					nCount = 0
					while true
						tMToken = lLexer.Lex()
						if ((tLast.eType == eTokenType.EndOfLineToken and tMToken.eType == eTokenType.PreprocessorToken) and tMToken.sText == "#endif") then
							if nCount == 0 then
								lSectionText.pop()
								break
							end if
							nCount = nCount - 1
						end if
						if tMToken.eType == eTokenType.EndOfFileToken then break end if
						if (tMToken.eType == eTokenType.PreprocessorToken and tMToken.sText == "#ifdef" or tMToken.sText == "#ifndef") then
							nCount = nCount + 1
						end if
						lSectionText.push(tMToken.sText)
						tLast = tMToken
					end while
					lSectionText.pull()
					
					cpCodeProcessor = new CodeProcessor
					cpCodeProcessor.Init(lSectionText.join(""), self.bShorten, self.bPreprocess)
					cpCodeProcessor.mMacros = self.mMacros
					sSection = cpCodeProcessor.Generate()

					for mMacro in cpCodeProcessor.mMacros
						self.mMacros[mMacro.key] = mMacro.value
					end for

					if sSection.len > 0 and sSection[sSection.len - 1] != char(10) or not IsAtEnd() then
						sSection = sSection + char(10)
					end if

					sProcessed = sProcessed + sSection
				else if tToken.sText == "#undef" then
					if lLexer.Lex().eType != eTokenType.WhitespaceToken then
						Error("Expected whitespace token")
					end if
					tIdentifier = lLexer.Lex()

					if tIdentifier.eType != eTokenType.IdentifierToken then
						Error("Not a identifier for macro: " + tIdentifier.sText)
					end if

					self.mMacros.remove(tIdentifier.sText)
				else if tToken.sText == "#resx" then
					if lLexer.Lex().eType != eTokenType.WhitespaceToken then
						Error("Expected whitespace token")
					end if
					tIdentifier = lLexer.Lex()

					if tIdentifier.eType != eTokenType.IdentifierToken then
						Error("Expected identifier token")
					end if

					if lLexer.Lex().eType != eTokenType.WhitespaceToken then
						Error("Expected whitespace token")
					end if

					tString = lLexer.Lex()

					if tString.eType != eTokenType.StringToken then
						Error("Not a string for macro: " + tIdentifier.sText)
					end if

					sPath = FixPath(tString.sText.replace("""", ""))
					fInclude = GetFile(sPath)
					if not fInclude then
						Error("Could not find file: " + sPath)
					end if

					lDirectResource = []
					for sLine in fInclude.get_content.split(char(10))
						lDirectResource.push("""" + sLine.replace("""","""""") + """")
					end for
					sRes = lDirectResource.join("+char(10)+")
					self.mMacros[tIdentifier.sText] = sRes
				else if tToken.sText == "#pragma" then
					if lLexer.Lex().eType != eTokenType.WhitespaceToken then
						Error("Expected whitespace token.")
					end if
					tOperation = lLexer.Lex()
					if tOperation.eType == eTokenType.IdentifierToken then
						if tOperation.sText == "once" then
							Warn("#pragma once is deprecated. (Replicate Behaivor: https://stackoverflow.com/a/4769354/13295377)")
						end if
					else if tOperation.eType == eTokenType.FunctionCallToken then
						if tOperation.sText == "lib" then
							tOperatorToken = lLexer.Lex()
							
							if tOperatorToken.eType == eTokenType.WhitespaceToken then
								tOperatorToken = lLexer.Lex()
								if tOperatorToken.eType != eTokenType.OperatorToken then
									Error("Expected '(' token. (Got: " + tOperatorToken.sText + ")")
								end if

								if tOperatorToken.sText[tOperatorToken.sText.len - 1] != "(" then
									Error("Expected '(' token. (Got: " + tOperatorToken.sText + ")")
								end if
							else if tOperatorToken.eType == eTokenType.OperatorToken then
								if tOperatorToken.sText[tOperatorToken.sText.len - 1] != "(" then
									Error("Expected '(' token. (Got: " + tOperatorToken.sText + ")")
								end if
							else
								Error("Expected '(' token. (Got: " + tOperatorToken.sText + ")")
							end if

							tLibraryName = lLexer.Lex()

							if tLibraryName.eType == eTokenType.WhitespaceToken then
								tLibraryName = lLexer.Lex()
								if tLibraryName.eType != eTokenType.IdentifierToken then
									Error("Expected identifier token. (Got: " + tLibraryName.sText + ")")
								end if
							else if tLibraryName.eType != eTokenType.IdentifierToken then
								Error("Expected identifier token. (Got: " + tLibraryName.sText + ")")
							end if

							tOperatorToken = lLexer.Lex()
							if tOperatorToken.eType == eTokenType.WhitespaceToken then
								tOperatorToken = lLexer.Lex()
								if tOperatorToken.eType != eTokenType.OperatorToken then
									Error("Expected ')' token. (Got: " + tOperatorToken.sText + ")")
								end if

								if tOperatorToken.sText[tOperatorToken.sText.len - 1] != ")" then
									Error("Expected ')' token. (Got: " + tOperatorToken.sText + ")")
								end if
							else if tOperatorToken.eType == eTokenType.OperatorToken then
								if tOperatorToken.sText[tOperatorToken.sText.len - 1] != ")" then
									Error("Expected ')' token. (Got: " + tOperatorToken.sText + ")")
								end if
							else
								Error("Expected ')' token. (Got: " + tOperatorToken.sText + ")")
							end if

							sProcessed = sProcessed + tLibraryName.sText + "=include_lib(""/lib/" + tLibraryName.sText + ".so"");if not " + tLibraryName.sText + " then;" + tLibraryName.sText + "=include_lib(current_path+""/" + tLibraryName.sText + ".so"");if not " + tLibraryName.sText + " then exit(""Couldn't load " + tLibraryName.sText + ".so"") end if;end if" + char(10)
						end if
					end if
				end if

				continue
			end if
			if tToken.eType == eTokenType.TabToken or tToken.eType == eTokenType.WhitespaceToken or tToken.eType == eTokenType.EndOfLineToken and self.bShorten == true then
				continue
			end if
			if tToken.eType == eTokenType.CommentToken and self.bShorten == true then
				lLexer.Lex()
				continue
			end if
		end if

		if tToken.eType == eTokenType.CommentToken and self.bShorten == true then
			continue
		end if

		if tToken.eType == eTokenType.IdentifierToken then
			if self.mMacros.hasIndex(tToken.sText) == true then
				tToken.sText = self.mMacros[tToken.sText]
			end if
		end if

		if tToken.eType == eTokenType.EndOfLineToken then
			if lLexer.Lookahead(1).eType == eTokenType.EndOfFileToken then
				break
			end if
		end if

		tLastToken = tToken
		sProcessed = sProcessed + tToken.sText
	end while

	return sProcessed
end function
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Script
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if not GetFile(sSynapsePath) then
	cComputer.create_folder(parent_path( sSynapsePath ), "synapse")
end if

cfgColorSettings = AssertiveConfWriter.AssertConfiguration( sSynapsePath + "color.conf")
AssertiveConfWriter.PushConf( cfgColorSettings )

AssertiveConfWriter.Write("ERROR_COLOR",   "#ff0000")
AssertiveConfWriter.Write("WARN_COLOR",    "#ffa500")
AssertiveConfWriter.Write("SUCCESS_COLOR", "#00bf33")

AssertiveConfWriter.WriteLine()

AssertiveConfWriter.WriteComment("Syntax Highlighting Palette")
AssertiveConfWriter.Write("CON_SYNTAX_OPERATOR",     rgb(102, 255, 204))
AssertiveConfWriter.Write("CON_SYNTAX_NUMBER",       rgb(255, 198, 0))
AssertiveConfWriter.Write("CON_SYNTAX_STRING",       rgb(173, 241, 149))
AssertiveConfWriter.Write("CON_SYNTAX_COMMENT",      rgb(102, 102, 102))
AssertiveConfWriter.Write("CON_SYNTAX_KEYWORD",      rgb(248, 109, 124))
AssertiveConfWriter.Write("CON_SYNTAX_BUILTIN",      rgb(132, 214, 247))
AssertiveConfWriter.Write("CON_SYNTAX_IDENTIFIER",   rgb(255, 255, 255))
AssertiveConfWriter.Write("CON_SYNTAX_FUNCCALL",     rgb(255, 255, 0))
AssertiveConfWriter.Write("CON_SYNTAX_LINENUM",      rgb(102, 102, 102))
AssertiveConfWriter.Write("CON_SYNTAX_PREPROCESSOR", rgb(102, 102, 102))
AssertiveConfWriter.WriteComment("Valid Tokens (Entering anything else will crash, also don't put spaces in between the '|'): OperatorToken, KeywordToken, BuiltinToken, IdentifierToken, NumberToken, CommentToken, StringToken, FunctionCallToken, PreprocessorToken")
AssertiveConfWriter.Write("CON_SYNTAX_BOLDED",     "StringToken|KeywordToken|BuiltinToken")

AssertiveConfWriter.WriteLine()

AssertiveConfWriter.WriteComment("Help Command Palette")
AssertiveConfWriter.Write("HELP_COMMAND_PLUS",      rgb(19, 144, 156))
AssertiveConfWriter.Write("HELP_COMMAND_ARGUMENTS", rgb(0, 191, 51))

AssertiveConfWriter.WriteLine()

AssertiveConfWriter.WriteComment("Version Command Palette")
AssertiveConfWriter.Write("VERSION_COMMAND_NAME",    rgb(173, 241, 149))
AssertiveConfWriter.Write("VERSION_COMMAND_VERSION", rgb(132, 214, 247))
AssertiveConfWriter.Write("VERSION_COMMAND_PATH",    rgb(255, 198, 0))

AssertiveConfWriter.PopConf()

cfgSettings = AssertiveConfWriter.AssertConfiguration( sSynapsePath + "settings.conf")
AssertiveConfWriter.PushConf(cfgSettings)

AssertiveConfWriter.Write("ISTAGE_SYNTAX_HIGHLIGHTING", "TRUE")
//AssertiveConfWriter.Write("ISTAGE_CHECK_VERSION",       "TRUE")

AssertiveConfWriter.PopConf()

CON_SYNTAX_OPERATOR = cfgColorSettings.GetSetting( "CON_SYNTAX_OPERATOR" )
CON_SYNTAX_NUMBER = cfgColorSettings.GetSetting( "CON_SYNTAX_NUMBER" )
CON_SYNTAX_STRING = cfgColorSettings.GetSetting( "CON_SYNTAX_STRING" )
CON_SYNTAX_COMMENT = cfgColorSettings.GetSetting( "CON_SYNTAX_COMMENT" )
CON_SYNTAX_KEYWORD = cfgColorSettings.GetSetting( "CON_SYNTAX_KEYWORD" )
CON_SYNTAX_IDENTIFIER = cfgColorSettings.GetSetting( "CON_SYNTAX_IDENTIFIER" )
CON_SYNTAX_FUNCCALL = cfgColorSettings.GetSetting( "CON_SYNTAX_FUNCCALL" )
CON_SYNTAX_BUILTIN = cfgColorSettings.GetSetting( "CON_SYNTAX_BUILTIN" )
CON_SYNTAX_BOLDED = cfgColorSettings.GetSetting( "CON_SYNTAX_BOLDED" ).split("\|")
CON_SYNTAX_PREPROCESSOR = cfgColorSettings.GetSetting( "CON_SYNTAX_PREPROCESSOR" )

//lKeywords  = ["if", "then", "else", "end", "function", "while", "true", "false", "for", "return", "not"]
//lOperators = ["=", ":", "-", "*", "%", "[", "]", "{", "}", "(", ")", "@", ";"]
//lNumbers   = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
//lBuiltins  = ["typeof", "print", "get_router", "get_switch", "nslookup", "clear_screen", "active_user", "home_dir", "get_shell", "user_input", "include_lib", "exit", "user_mail_address", "user_bank_number", "whois", "wait", "command_info", "program_path", "current_path", "launch_path", "format_columns", "current_date", "is_lan_ip", "parent_path", "is_valid_ip", "bitwise", "import_code"]
//mFixes = {BoldCast(ColorCast(CON_SYNTAX_BUILTIN, BoldCast(ColorCast(CON_SYNTAX_KEYWORD, "for")) + "mat_columns")) : BoldCast(ColorCast(CON_SYNTAX_BUILTIN, "format_columns"))}
//ssString   = """"
//sComment   = "//"

LineString = function(sCode)
	//sFinal = ""
	//nLine = 1
	//for sStr in sCode.split(char(10))
	//	sFinal = sFinal + ColorCast(cfgColorSettings.GetSetting("CON_SYNTAX_LINENUM"), nLine + " |  ") + sStr + char(10)

	//	nLine = nLine + 1
	//end for
	sFinal = ""
	lLines = sCode.split(char(10))
	nLastLine = lLines.len
	nLastLineLen = str(nLastLine).len

	nLine = 1
	for sStr in lLines
		sFinal = sFinal + "  "
		nDif = nLastLineLen - str(nLine).len
		
		if nDif == 0 then
			sFinal = sFinal + ColorCast(cfgColorSettings.GetSetting("CON_SYNTAX_LINENUM"), nLine) + "   " + sStr + char(10)
		else
			for i in range(1, nDif)
				sFinal = sFinal + " "
			end for
			sFinal = sFinal + ColorCast(cfgColorSettings.GetSetting("CON_SYNTAX_LINENUM"), nLine) + "   " + sStr + char(10)
		end if

		nLine = nLine + 1
	end for

	return sFinal
end function

Type2Color = function(eType)
	if eType == eTokenType.OperatorToken then
		return CON_SYNTAX_OPERATOR
	else if eType == eTokenType.NumberToken then
		return CON_SYNTAX_NUMBER
	else if eType == eTokenType.StringToken then
		return CON_SYNTAX_STRING
	else if eType == eTokenType.CommentToken then
		return CON_SYNTAX_COMMENT
	else if eType == eTokenType.KeywordToken then
		return CON_SYNTAX_KEYWORD
	else if eType == eTokenType.BuiltinToken then
		return CON_SYNTAX_BUILTIN
	else if eType == eTokenType.FunctionCallToken then
		return CON_SYNTAX_FUNCCALL
	else if eType == eTokenType.IdentifierToken then
		return CON_SYNTAX_IDENTIFIER
	else if eType == eTokenType.PreprocessorToken then
		return CON_SYNTAX_PREPROCESSOR
	end if

	return ""
end function

HighlightString = function(sCode)
	lLexer = new GSLexer
	lLexer.Init(sCode)
	sHighlighted = ""

	while true
		tToken = lLexer.Lex()

		if not tToken.eType != eTokenType.EndOfFileToken then break end if
		if tToken.eType == eTokenType.WhitespaceToken or tToken.eType == eTokenType.UnkownToken then
			sHighlighted = sHighlighted + tToken.sText
			continue
		end if
		ccColor = Type2Color(tToken.eType)
		
		sText = tToken.sText.replace("\",ColorCast(ccColor, "\"))
		sToken = ColorCast(ccColor, sText)
		
		if CON_SYNTAX_BOLDED.indexOf(TokenType2String(tToken.eType)) != null then
			sToken = BoldCast(sToken)
		end if

		sHighlighted = sHighlighted + sToken
	end while

	return sHighlighted
end function

// This is honestly the worst syntax highlight function
// that has ever been summoned into reality
// Someone optimise this function when I upload to github pls

// 2022 H3xad3cimal here, I finally came along and added an actual Lexer for the Syntax Highlighting
//HighlightString = function(sCode)
//	//for num in lNumbers
//	//	sCode = sCode.replace(num, ColorCast(CON_SYNTAX_NUMBER, num))
//	//end for
//
//	for operator in lOperators
//		sCode = sCode.replace(operator, ColorCast(CON_SYNTAX_OPERATOR, operator))
//	end for
//
//	for builtin in lBuiltins
//		sCode = sCode.replace(builtin, BoldCast(ColorCast(CON_SYNTAX_BUILTIN, builtin)))
//	end for
//
//	for keyword in lKeywords
//		sCode = sCode.replace(keyword, BoldCast(ColorCast(CON_SYNTAX_KEYWORD, keyword)))
//	end for
//
//	for fix in mFixes
//		sCode = sCode.replace(fix.key, fix.value)
//	end for
//
//	sCode = sCode.replace(ssString, BoldCast(ColorCast(CON_SYNTAX_STRING, ssString)))
//	sCode = sCode.replace(sComment, BoldCast(ColorCast(CON_SYNTAX_COMMENT, sComment)))
//
//	return sCode
//end function

CodifyString = function(sCode)
	return LineString(HighlightString(sCode))
end function

Error = function(message)
	exit("<b>" + ColorCast(cfgColorSettings.GetSetting("ERROR_COLOR"), message))
end function

Warn = function(message)
	print(BoldCast(ColorCast(cfgColorSettings.GetSetting("WARN_COLOR"), "WARNING: ")) + message)
end function

IsProjectFolder = function(sPath)
	fProjectConf = GetFile(sPath + "/project.conf")
	if not fProjectConf then return false end if
	cfgProjectConf = new Configuration
	cfgProjectConf.Init( sPath + "/project.conf" )

	if not cfgProjectConf.HasSetting(  "proj_name"   ) then return false end if
	if not cfgProjectConf.HasSetting( "proj_version" ) then return false end if

	if not cfgProjectConf.HasSetting( "bin_location" ) then return false end if
	if not cfgProjectConf.HasSetting( "lib_location" ) then return false end if
	if not cfgProjectConf.HasSetting( "obj_location" ) then return false end if

	return true
end function

IsInProjectFolder = function()
	return IsProjectFolder(current_path)
end function

CreateProjectConfiguration = function(sPath, sName)
	cfgProjectConf = AssertiveConfWriter.AssertConfiguration(sPath + "/project.conf")
	AssertiveConfWriter.PushConf( cfgProjectConf )

	// # Metadata
	// proj_name=PROJECT_NAME
	// proj_version=1.0
	AssertiveConfWriter.WriteComment( "Metadata" )
	AssertiveConfWriter.Write( "proj_name"    , sName )
	AssertiveConfWriter.Write( "proj_version" , "1.0")

	AssertiveConfWriter.WriteLine()

	// # Locations
	// bin_location=sPath + "/bin"
	// lib_location=sPath + "/lib"
	// obj_location=sPath + "/obj"
	AssertiveConfWriter.WriteComment( "Locations" )
	AssertiveConfWriter.Write( "bin_location" , sPath + "/bin" )
	AssertiveConfWriter.Write( "lib_location" , sPath + "/lib" )
	AssertiveConfWriter.Write( "obj_location" , sPath + "/obj")

	AssertiveConfWriter.PopConf()

	return cfgProjectConf
end function

GetProjectConfiguration = function()
	cfgProjectConf = new Configuration
	cfgProjectConf.Init( current_path + "/project.conf" )
	return cfgProjectConf
end function

GetProjectConfigurationFromPath = function(sPath)
	cfgProjectConf = new Configuration
	cfgProjectConf.Init( sPath + "/project.conf" )
	return cfgProjectConf
end function

//////////////////////////
import_code("/usr/tmp/commands.src")

main = function()
	lParamCopy = mew(params)
	sCommandName = lParamCopy.pull()

	if not mCommandList.hasIndex(sCommandName) then
		Error("No option found called: " + sCommandName)
	end if

	mCommandList[sCommandName].Execute(lParamCopy)
end function

main()
