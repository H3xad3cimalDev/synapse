arg_equals = function(args, argc)
	if not args.len == argc then
		Error(params[0] + ": please pass " + argc + " argument.")
	end if
end function

//////////////////////////////////////////////////////////////
// Commands
//////////////////////////////////////////////////////////////
VersionCommand = mew(Command)

VersionCommand.Init = function()
	self.__init("version", "Display's the project's info.", [])
end function

VersionCommand.Execute = function(Args)
	if not IsInProjectFolder() then
		Error("Not in project path")
	end if

	cfgProjectConf = GetProjectConfiguration()
	sbColumns = new StringBuilder

	c = function(colorconf, sconf)
		return BoldCast(ColorCast(cfgColorSettings.GetSetting( "VERSION_COMMAND_" + colorconf ), cfgProjectConf.GetSetting( sconf )))
	end function

	sbColumns.AppendLine( "Name "    + c( "NAME"    , "proj_name" ))
	sbColumns.AppendLine( "Version " + c( "VERSION" , "proj_version" ) )
	sbColumns.AppendLine( "Binary "  + c( "PATH"    , "bin_location") )
	sbColumns.AppendLine( "Library " + c( "PATH"    , "lib_location") )
	sbColumns.Append(     "Object "  + c( "PATH"    , "obj_location") )

	print(format_columns( sbColumns.ToString() ))
end function

//////////////////////////////////////////////////////////////
NewCommand = mew(Command)

NewCommand.Init = function()
	self.__init("new", "Creates a new project with the name passed.", ["NAME"])
end function

NewCommand.Execute = function(Args)
	arg_equals(Args, 1)

	if GetFile(FixPath(Args[0])) then
		Error("Directory already exists.")
	end if

	print("\nCreating project """ + Args[0] + """.\n")
	Timer.Start()
	cComputer.create_folder(current_path, Args[0])
	sProjectPath = current_path + "/" + Args[0]

	print("Running 'project-setup' on " + sProjectPath + "...")
	print("  Creating Binary Location " + sProjectPath + "/bin...")
	cComputer.create_folder(sProjectPath, "bin")
	print("  Creating Library Location " + sProjectPath + "/lib...")
	cComputer.create_folder(sProjectPath, "lib")
	print("  Creating Object Location " + sProjectPath + "/obj...")
	cComputer.create_folder(sProjectPath, "obj")
	cComputer.create_folder(sProjectPath + "/obj", "hash")
    cComputer.create_folder(sProjectPath + "/obj", "preprocess")
	cComputer.create_folder(sProjectPath + "/obj", "checks")
	cComputer.create_folder(sProjectPath + "/obj", "src")
	print("  Creating Main file " + sProjectPath + "/Main.src...")
	cComputer.touch(sProjectPath, "Main.src")
	GetFile(sProjectPath + "/Main.src").set_content(sDefaultMain)

	print("  Generating project configuration file " + sProjectPath + "/project.conf...")
	CreateProjectConfiguration(sProjectPath, Args[0])
	print("  Generating build configuration file " + sProjectPath + "/build.conf...")
	BuildSystem.Init( sProjectPath )
	BuildSystem.GenerateConfiguration()

	print("  Creation completed in " + Timer.StopMilli() + " ms for " + sProjectPath + ".")
	print("\nCreation succeeded.\n")
end function

//////////////////////////////////////////////////////////////
BuildCommand = mew(Command)

BuildCommand.Init = function()
	self.__init("build", "Builds the current project.", [])
end function

BuildCommand.Execute = function(Args)
	sPath = null
	if not IsInProjectFolder() then	
		if not IsProjectFolder(parent_path(current_path)) then
			sPath = parent_path(current_path)
		else
			Error("Not in project path")
		end if
	else
		sPath = current_path
	end if
	
	BuildSystem.Init( sPath )
	BuildSystem.Build()
end function

//////////////////////////////////////////////////////////////
RunCommand = mew(Command)

RunCommand.Init = function()
	self.__init("run", "Builds and runs the current project.", ["PROGRAM_ARGS"])
end function

RunCommand.Execute = function(Args)
	sPath = null
	
	if not IsInProjectFolder() then
		if not IsProjectFolder(parent_path(current_path)) then
			sPath = parent_path(current_path)
		end if
		Error("Not in project path")
	else
		sPath = current_path
	end if
	
	BuildSystem.Init( sPath )
	BuildSystem.Build()

	cfgProjectConf = GetProjectConfiguration()
	btProgram = BinTools.CreateBinaryTool(cfgProjectConf.GetSetting("bin_location") + "/" + cfgProjectConf.GetSetting("proj_version") + "/" + cfgProjectConf.GetSetting("proj_name"))
	btProgram.Launch(Args.join(" "))
end function

//////////////////////////////////////////////////////////////
GenerateBuidListCommand = mew(Command)

GenerateBuidListCommand.Init = function()
	self.__init("genlbl", "Generates library build file.", [])
end function

GenerateBuidListCommand.Execute = function(Args)
	Timer.Start()

	sUsePath = null
	if Args.len > 0 then
		if not GetFile(Args[0]) then
			Error(Args[0] + " not a valid path.")
		end if
		sUsePath = Args[0]
	else
		sUsePath = current_path
	end if

	print("Generating " + sUsePath + "/lib_build.list ")
	BuildSystem.Init( sUsePath )
	BuildSystem.GenerateLibraryBuildList()
	print("Done. (" + Timer.StopMilli() + " ms)")
end function

//////////////////////////////////////////////////////////////
LogsCommand = mew(Command)

LogsCommand.Init = function()
	self.__init("logs", "Display's the build logs.", [])
end function

LogsCommand.Execute = function(Args)
	if not IsInProjectFolder() then	
		Error("Not in project path")
	end if

	cfgProjectConf = GetProjectConfiguration()

	if not GetFile(cfgProjectConf.GetSetting("obj_location") + "/build.log") then
		Error("No build logs found")
	end if

	print(GetFile(cfgProjectConf.GetSetting("obj_location") + "/build.log").get_content)
end function

//////////////////////////////////////////////////////////////
ExportCommand = mew(Command)

ExportCommand.Init = function()
	self.__init("export", "Exports project.", [])
end function

ExportCommand.Execute = function(Args)
	ExportSystem.Init( current_path )
	ExportSystem.Export(true)
end function

//////////////////////////////////////////////////////////////
ManifestCommand = mew(Command)

ManifestCommand.Init = function()
	self.__init("manifest", "Converts a SEF file into a project.", ["SEF_PATH"])
end function

ManifestCommand.Execute = function(Args)
	fSEF = GetFile(Args[0])
	if not fSEF then
		fSEF = GetFile(FixPath(Args[0]))
		if not fSEF then
			Error("Invalid path: " + Args[0])
		end if
	end if

	print("Executing SEF Script...")
	ExportSystem.Evaluate(fSEF.get_content, "  ", current_path)
end function

//////////////////////////////////////////////////////////////
IStageCommand = mew(Command)

IStageCommand.Init = function()
	self.__init("istage", "Displays the files that have changed since last time the project was built.", [])
end function

IStageCommand.Execute = function(Args)
	if not IsInProjectFolder() then
		Error("Not in project path.")
	end if

	print("Loading hashes...")
	cfgProjectConf = GetProjectConfiguration()

	fHashes = GetFile(cfgProjectConf.GetSetting("obj_location") + "/hash/" + cfgProjectConf.GetSetting("proj_version") + ".hashes.list")

	if not fHashes then
		Error("No hash file found.")
	end if

	lfHashes = new ListFile
	lfHashes.Init( fHashes )
	
	lHashes = lfHashes.GetList()

	print("Detecting file changes...")
	lFiles = []

	for sFHash in lHashes
		lHashData = sFHash.split(":")
		sFileName = lHashData[0]
		sHash = lHashData[1]

		if sFileName == "Main.src" then
			fMain = GetFile(current_path + "/Main.src")

			if not HashFile(fMain) == sHash then
				lFiles.push(fMain.path)
			end if
		else
			fSrc = GetFile(cfgProjectConf.GetSetting("lib_location") + "/" + sFileName)

			if not HashFile(fSrc) == sHash then
				lFiles.push(fSrc.path)
			end if
		end if
	end for

	if lFiles.len == 0 then
		exit("No changes detected.")
	else
		print(lFiles.len + " changes detected. Would you like to review the files?")
		sInput = user_input("y/n [No by default]: ", false)

		if sInput.len > 0 and sInput[0].lower == "y" then
			for sPath in lFiles
				clear_screen
				fFile = GetFile(sPath)
				print("Name: " + fFile.name + "  |  Path: " + fFile.path + "  |  Lines: " + fFile.get_content.split(char(10)).len + "  |  Characters: " + fFile.get_content.len)
				if cfgSettings.GetSetting("ISTAGE_SYNTAX_HIGHLIGHTING").upper == "TRUE" then
					print(CodifyString(fFile.get_content))
				else
					print(fFile.get_content)
				end if

				user_input("Press enter for next file.", false)
			end for
			clear_screen
		end if
	end if
end function

//////////////////////////////////////////////////////////////
HelpCommand = mew(Command)

HelpCommand.Init = function()
	self.__init("help", "", [])
end function

HelpCommand.Execute = function(Args)
	sbColumns = new StringBuilder

	for cCommand in mCommandList.values
		if cCommand.sName == "help" or cCommand.sName == "dev" then
			continue
		end if

		sbColumns.Append(ColorCast(cfgColorSettings.GetSetting("HELP_COMMAND_PLUS"), "+"))
		sbColumns.Append(cSpace)
		sbColumns.Append(BoldCast(cCommand.sName))
		sbColumns.Append(" ")
		sbColumns.Append(cCommand.sDesc)
		if cCommand.lArgs.len > 0 then
			sbColumns.Append(" ")
			sbColumns.AppendLine(ColorCast(cfgColorSettings.GetSetting("HELP_COMMAND_ARGUMENTS"), cCommand.lArgs.join("," + cSpace)))
		else
			sbColumns.Append(char(10))
		end if
	end for
	
	sFormated = cformat_columns(sbColumns.ToString())
	print(slice(sFormated, 0, sFormated - 1))
end function

//////////////////////////////////////////////////////////////
DevTestCommands = mew(Command)

DevTestCommands.Init = function()
	self.__init("dev", "", [])
end function

DevTestCommands.Execute = function(Args)
	if Args[0] == "hash" then
		fFile = GetFile(Args[1])
		if not fFile then
			fFile = GetFile(FixPath(Args[1]))
			if not fFile then
				Error("Invalid path: " + Args[1])
			end if
		end if

		print(fFile.name + ": " + HashFile(fFile))
	else if Args[0] == "preprocess" then
		cpCodeProcessor = new CodeProcessor
		cpCodeProcessor.Init(GetFile(FixPath(Args[1])).get_content, true, true)

		print(cpCodeProcessor.Generate())
	end if
end function

//////////////////////////////////////////////////////////////
/////////////////////////// SYNX //////////////////////////
mSynxCommandList = {}

ImplementSynxCommand = function(typeClass)
	cmd = new typeClass
	cmd.Init()
	mSynxCommandList[cmd.sName] = cmd
end function
///////////////////////////////////////////////////////////
SynxInitCommand = mew(Command)

SynxInitCommand.Init = function()
	self.__init("init", "Initiliazes a synx config", ["HOST_IP", "SYNX_PASSWORD", "HOST_PORT", "PROJECT_NAME"])
end function

SynxInitCommand.Execute = function(Args)
	if Args.len < 4 then
		Error("You need to pass host's IP, synx password, and the host port")
	end if

	sHost = Args[0]
	sPassword = Args[1]
	nPort = Args[2].to_int
	sProjectName = Args[3]

	sSynxConfig = new SynxConfig
	sSynxConfig.New( current_path, sHost, nPort, sPassword, sProjectName )
end function
///////////////////////////////////////////////////////////
SynxSetupServerCommand = mew(Command)

SynxSetupServerCommand.Init = function()
	self.__init("setup", "Setups the current computer as a Synx Server. (Requires root)", ["SYNX_SERVER_PASSWORD"])
end function

SynxSetupServerCommand.Execute = function(Args)
	if Args.len != 1 then
		Error("Please pass a password (No spaces).")
	end if
	
	sSynxServer = new SynxServer
	sSynxServer.SetupServer(Args[0])
end function
///////////////////////////////////////////////////////////
SynxBranchCommand = mew(Command)

SynxBranchCommand.Init = function()
	self.__init("branch", "Branch managing.", ["OPTION", "ARGS"])
end function

SynxBranchCommand.Execute = function(Args)
	if not cComputer.File(current_path + "/.synx") then
		Error("No Synx Configuration initiated")
	end if

	if Args.len < 1 then
		Error("Please pass atleast one argument.")
	end if

	ssySynxConfig = new SynxConfig
	ssySynxConfig.Init( current_path )
	sssSynxServer = ssySynxConfig.Connect()
	if not sssSynxServer.ProjectExists(ssySynxConfig.sProjectName) then
		Error("Project doesn't exist on the server.")
	end if
	spProject = sssSynxServer.GetProject(ssySynxConfig.sProjectName)

	sOption = Args.pull()

	if sOption == "list" then
		for branch in spProject.lBranches
			print("| --> " + branch)
		end for
	else if sOption == "create" then
		if Args.len != 1 then
			Error("Please pass in a branch name. (One word)")
		end if

		sBranch = Args.pull()
		if spProject.BranchExists(sBranch) then
			print("Branch -> " + sBranch + " | Already exists.")
		end if

		spProject.CreateBranch(sBranch)
		print("Created branch -> " + sBranch)
	else if sOption == "delete" then
		if Args.len != 1 then
			Error("Please pass in a branch name. (One word)")
		end if

		sBranch = Args.pull()
		if not spProject.BranchExists(sBranch) then
			Error("Branch -> " + sBranch + " | Doesn't exists.")
		end if

		spProject.RemoveBranch(sBranch)
		print("Deleted branch -> " + sBranch)
	else if sOption == "set" then
		if Args.len != 1 then
			Error("Please pass in a branch name. (One word)")
		end if

		sBranch = Args.pull()
		if not spProject.BranchExists(sBranch) then
			Error("Branch -> " + sBranch + " | Doesn't exists.")
		end if

		ssySynxConfig.cfgSynxConfig.SetSetting("BRANCH", sBranch)
		ssySynxConfig.cfgSynxConfig.Save()
		print("Working on Branch -> " + sBranch)
	else
		print("<b>Usages</b>:")

		print(char(9) + "list            | Prints out the branches")
		print(char(9) + "create <branch> | Creates a new branch")
		print(char(9) + "delete <branch> | Deletes a branch")
		print(char(9) + "set <branch>    | Sets a branch as the working branch")
	end if
end function
///////////////////////////////////////////////////////////
SynxPushCommand = mew(Command)

SynxPushCommand.Init = function()
	self.__init("push", "Pushes this current project to the branch.", [])
end function

SynxPushCommand.Execute = function(Args)
	if not cComputer.File(current_path + "/.synx") then
		Error("No Synx Configuration initiated")
	end if

	sSynxConf = cComputer.File(current_path + "/.synx/server.conf").get_content
	ssySynxConfig = new SynxConfig
	ssySynxConfig.Init( current_path )

	sssSynxServer = ssySynxConfig.Connect()

	if not sssSynxServer.ProjectExists(ssySynxConfig.sProjectName) then
		Error("Project doesn't exist on the server.")
	end if

	if not IsInProjectFolder() then
		Error("You can only push Synapse Projects.")
	end if

	ExportSystem.Init( current_path )
	lSef = ExportSystem.Export(false)
	print("Saving it to server...")
	spSynxProject = sssSynxServer.GetProject(ssySynxConfig.sProjectName)
	spSynxProject.SetSef(ssySynxConfig.sCurrentBranch, lSef.join(char(10)))
	print("Done\n")
end function
///////////////////////////////////////////////////////////
SynxPullCommand = mew(Command)

SynxPullCommand.Init = function()
	self.__init("pull", "Pulls the current SEF in the branch", [])
end function

SynxPullCommand.Execute = function(Args)
	if not cComputer.File(current_path + "/.synx") then
		Error("No Synx Configuration initiated")
	end if

	sSynxConf = cComputer.File(current_path + "/.synx/server.conf").get_content
	ssySynxConfig = new SynxConfig
	ssySynxConfig.Init( current_path )

	sssSynxServer = ssySynxConfig.Connect()

	if not sssSynxServer.ProjectExists(ssySynxConfig.sProjectName) then
		Error("Project doesn't exist on the server.")
	end if

	print("Are you sure you want to do this? This will replace the current folder with the Synapse project in branch: " + ssySynxConfig.sCurrentBranch)
	sInput = user_input("y/n [No by default]: ", false)

	if sInput.len > 0 and sInput[0].lower == "y" then
		spSynxProject = sssSynxServer.GetProject(ssySynxConfig.sProjectName)
		sSefFile = spSynxProject.GetSef(ssySynxConfig.sCurrentBranch)
		GetFile(current_path).delete()

		ExportSystem.Evaluate(sSefFile, "  ", parent_path(current_path))
		cComputer.create_folder(current_path, ".synx")
		cComputer.touch(current_path + "/.synx", "server.conf")
		GetFile(current_path + "/.synx/server.conf").set_content(sSynxConf)
	end if
end function

////////////////////////////////////////////////////////////
SynxMakeCommand = mew(Command)

SynxMakeCommand.Init = function()
	self.__init("make", "Creates the project on the server", [])
end function

SynxMakeCommand.Execute = function(Args)
	if not cComputer.File(current_path + "/.synx") then
		Error("No Synx Configuration initiated")
	end if

	ssySynxConfig = new SynxConfig
	ssySynxConfig.Init( current_path )

	sssSynxServer = ssySynxConfig.Connect()
	sssSynxServer.CreateProject(ssySynxConfig.sProjectName)
end function
///////////////////////////////////////////////////////////
SynxDeleteCommand = mew(Command)

SynxDeleteCommand.Init = function()
	self.__init("delete", "Deletes the current project on the server", [])
end function

SynxDeleteCommand.Execute = function(Args)
	if not cComputer.File(current_path + "/.synx") then
		Error("No Synx Configuration initiated")
	end if

	ssySynxConfig = new SynxConfig
	ssySynxConfig.Init( current_path )

	sssSynxServer = ssySynxConfig.Connect()

	if not sssSynxServer.ProjectExists(ssySynxConfig.sProjectName) then
		Error("Project doesn't exist.")
	end if

	sssSynxServer.RemoveProject(ssySynxConfig.sProjectName)
end function
///////////////////////////////////////////////////////////
SynxHelpCommand = mew(Command)

SynxHelpCommand.Init = function()
	self.__init("help", "", [])
end function

SynxHelpCommand.Execute = function(Args)
	sbColumns = new StringBuilder

	for cCommand in mSynxCommandList.values
		if cCommand.sName == "help" then
			continue
		end if

		sbColumns.Append(ColorCast(cfgColorSettings.GetSetting("HELP_COMMAND_PLUS"), "+"))
		sbColumns.Append(cSpace)
		sbColumns.Append(BoldCast(cCommand.sName))
		sbColumns.Append(" ")
		sbColumns.Append(cCommand.sDesc)
		if cCommand.lArgs.len > 0 then
			sbColumns.Append(" ")
			sbColumns.AppendLine(ColorCast(cfgColorSettings.GetSetting("HELP_COMMAND_ARGUMENTS"), cCommand.lArgs.join("," + cSpace)))
		else
			sbColumns.Append(char(10))
		end if
	end for
	sFormated = cformat_columns(sbColumns.ToString())
	print(slice(sFormated, 0, sFormated - 1))
end function
///////////////////////////////////////////////////////////////
ImplementSynxCommand(SynxHelpCommand)
ImplementSynxCommand(SynxInitCommand)
ImplementSynxCommand(SynxSetupServerCommand)
ImplementSynxCommand(SynxPullCommand)
ImplementSynxCommand(SynxMakeCommand)
ImplementSynxCommand(SynxBranchCommand)
ImplementSynxCommand(SynxPushCommand)
ImplementSynxCommand(SynxDeleteCommand)

SynxCommands = mew(Command)

SynxCommands.Init = function()
	self.__init("synx", "Synx Server Manager", ["OPTIONS", "ARGS"])
end function

SynxCommands.Execute = function(Args)
	if Args.len < 1 then
		Error("Please pass a synx command.")
	end if

	lParamCopy = mew(Args)
	sCommandName = lParamCopy.pull()

	if not mSynxCommandList.hasIndex(sCommandName) then
		Error("No option found called: " + sCommandName)
	end if

	mSynxCommandList[sCommandName].Execute(lParamCopy)
end function

//////////////////////////////////////////////////////////////
// Command Loading
mCommandList = {}

ImplementCommand = function(typeClass)
	cmd = new typeClass
	cmd.Init()
	mCommandList[cmd.sName] = cmd
end function

ImplementCommand(NewCommand)
ImplementCommand(IStageCommand)
ImplementCommand(VersionCommand)
ImplementCommand(BuildCommand)
ImplementCommand(RunCommand)
ImplementCommand(GenerateBuidListCommand)
ImplementCommand(LogsCommand)
ImplementCommand(HelpCommand)
ImplementCommand(ExportCommand)
ImplementCommand(ManifestCommand)
ImplementCommand(DevTestCommands)
ImplementCommand(SynxCommands)