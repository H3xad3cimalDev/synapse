# Synapse
Intelligent GreyHack Project Managing

## What is it?
Synapse is a project manager I (`H3xad3cimal`) have been working on for a long time, it has many features.

## Features
- [x] Auto building
- [x] Stage checking
- [x] Console Syntax Highlighting
- [x] Exporting projects into one file that can be loaded by Synapse
- [x] Code minifier
- [x] Easily Configurable
- [x] Project managing on a different server.
- [x] Preprocessor

## How do I install it?
Create a folder in `/usr` called `tmp` and paste `synapse_commands.lua` in this repo and name it

`synapse_commands.src` and then goto `synapse.lua` and just goto the CodeEditor and build it

and save it in `/bin`

## How to use
Once you have Synapse installed let's create a basic project!

### Configurations
Most settings in Synapse use a `.conf` file format that looks something like this
```conf
# Comment
SETTING_NAME=SETTING_VALUE
```

Which is important to realize to be good at using Synapse

```
NOTE: Using the comment character (#) like this

SETTING_INIT=SETTING_VALUE # Some comment

WILL NOT WORKS AND THE SETTING VALUE WILL INCLUDE THE COMMENT
```

### Creating a Project
In order to create a project we're going to utilize the `new` command

Enter this to create a project. (Recommended only to use alpahabet characters, no characters such as `- , _ .` or anything that a file can't be named)
```console
$ synapse new ProjectName
```

Once you ran that you can go into the project directory by using ```cd```.

If you use the `ls` you can see how Synapse Projects are structured
```console
$ ls
bin
lib
obj
Main.src
project.conf
build.conf
```

Let's talk a bit about project structuring
### Project structure
Let's talk about the folders.

We have
```
lib
bin
obj
```

We don't have to worry about the `obj` folder because that's raw data Synapse uses but the `lib`, and `bin` folder

are important to us.

So the `bin` folder is where the `binaries` are stored when compiling with Synapse and everything file in the `lib`

folder will be added to the `binary` at compile time. These folder directories can be changed in the `project.conf`

### Libraries
Every file in `lib` folder is considered a `library` with each of their contents being added onto the executeable.

`NOTE: The order of which libraries get added first can be controlled by a lib_build.list file check "Build System" Section for more info`

### Project Configuration
The Project Configuration is the `project.conf` file in a project structure, and it looks something like this

```conf
# Metadata
proj_name=ProjectName
proj_version=1.0

# Locations
bin_location=/home/USER/ProjectName/bin
lib_location=/home/USER/ProjectName/lib
obj_location=/home/USER/ProjectName/obj
```

Let's talk about the `Metadata` section which holds the settings `proj_name`, and `proj_version`

`proj_name` is just the project's name and what the binary is going to be named when compiled

`proj_version` is the folder the project is going to be held in the `bin` folder

Now the `Locations` section is where Synapse will look for the `bin`, `lib`, and `obj` folders.

### Build System
The "`Build System`" is a system in Synapse which handles `Code Generation` and compiling.

The `Build System` will look at every file in the `lib` folder as a library and add it into the `binary`

You can control the other in which it adds the files by creating a `lib_build.list` file by using the `genlbl` command like so

`Main.src` is the main file and always the last to be added on the `binary` no matter what

```console
$ synapse genlbl
```

In which you handle write the file names in the you want order

```text
lib2.src
lib1.src
```

#### Build Configuration
The Build Configuration is the `build.conf` file in a project structure, it tells the build system on how to generate the binary

It looks something like this by default

```conf
# CHECK_SRCS is used for checking if
# the libraries and the main.src for compiler
# errors
CHECK_SRCS=TRUE
# GEN_LOG generates a log
# in the obj_location
GEN_LOG=FALSE
# if you want to use the import_code
# function then set it to TRUE or do you want to slap each
# library ontop of each other keep it FALSE. (Usefull for bypassing character limit)
IMPORT=FALSE
# Makes the compiled file shorter by removing indents
# and comments and empty lines
SHORTEN_COMPILED_FILE=TRUE
```

We'll look at each setting step by step starting with

`CHECK_SRC` It tells the the build system to check for any non-compilable source code being passed in every file, if it does find source code which it can't compile it will stop building the project after checking every file.

`GEN_LOG` Generates logs or not recommend keeping this off right now since the logs aren't really descriptive currently and it takes up space.

`IMPORT` Instead of stitching the files on one and another the `Build System` uses the `import_code` function to build the code together.

`SHORTEN_COMPILED_FILE` Uses a feature that shortens code to a minimum. `NOTE: It will shorten the code files even if IMPORT is on by creating new shortened files and import does instead`

### Preprocessors
Synapse has tooken after `C/C++` and added Preprocessors to the language!

A Preprocessor syntax usually looks something like this!
```c++
#PRE_PROCESSOR_NAME <Arguments...>
```

`NOTE: The Preprocessor runs at compile time not Runtime`

`NOTE: Inorder to use any preprocessor you have to put '#' symbol at the very begining, NO WHITESPACES`

#### `Define` Processor
The define processor just like the `C/C++` Processor works by basically coping and pasting
whatever is in the "`Macro`"

Processor Syntax
```c++
#define PROCESSOR_NAME WHAT TO COPY
```

And when calling the processor Synapse will copy whatever is inside the `Macro`

`NOTE: You can define an empty macro simply but going to the next leaving the copy empty like this: #define EMPTY_MACRO`

Example:

```c++
/////////////// BEFORE PREPROCESSOR
#define HELLO "Hello World!"
print(HELLO)
////////////// AFTER PREPROCESSOR
print("Hello World!")
```

`NOTE: The key difference between Macros and Variables [Built into the language] is that Macros are basically copy and paste tools while Variables are what they are, values stored in memory`

Example of incorrect use of `Macros`
```c++
#define RANDOM_FLOAT rnd
randomFloat = rnd

print(randomFloat) // 1.73264
print(randomFloat) // 1.73264

print(RANDOM_FLOAT) // 2.3489738
print(RANDOM_FLOAT) // 3.3287487

///////////// AFTER PREPROCESSOR
randomFloat = rnd

print(randomFloat) // 1.73264
print(randomFloat) // 1.73264

print(rnd) // 2.3489738
print(rnd) // 3.3287487
```

### `Include` Processor
It copies the file contents and paste it into the current file, it also processes the file like any other code file

Syntax:
```c++
#include "code_file.src"
```

Example:
```c++
//////// Source1.src
print("Hello")
//////// Main.src
#include "Source1.src"
print("World")
/////// Output
Hello
World
```

### `IfDef` and `IfNDef` Processor
The `ifdef` and `ifndef` means the same but opposite, `ifdef` means "If Defined" and `ifndef` means "If Not Defined"

This is the Syntax

```c++
#ifdef MACRO
// code
#endif

#ifndef MACRO
// code
#endif
```

but how these macro works are pretty simply anything inside the processors will run if the requirements are set for example

```c++
#define MACRO
#ifdef MACRO
print("if the macro is defined")
#endif

#ifndef MACRO
print("if the macro is not defined")
#endif

///////// AFTER PROCESSING
print("if the macro is defined")
```

`NOTE: Always end every if with and #endif or else it will have unintended behaivor`

### `UnDef` Procesor
The `undef` processor means "Undefine"

How this processor works is that it will undefine a macro so all uses of the macro afterwards doesn't work

Syntax:

```c++
#undef MACRO_NAME
```

Example:
```c++
#define MACRO

#undef MACRO
#ifndef MACRO
print("No macro")
#endif
///////////// AFTER PROCESSING
print("No macro")
```

### `Resx` Processor
The `resx` processor means "Resource"

How this processor works is that it will take whatever file path you pass to it copy its contents into a macro

Syntax:
```c++
#resx FILE "/path/to/file"
```

Example:
```c++
//////// Test.txt
Test File
Hello
H
I
//////// Source Code

#resx TEST_FILE "Test.txt"
print(TEST_FILE)

/////// Output
Test File
Hello
H
I
```

`NOTE: It doesn't get the file contents on runtime and gets the file contents on compile time like the "define" processor`

### Compiling & Running
You can compile your project using the `build` command like so.

```console
$ synapse build
```

Then you can goto `bin/PROJECT_VERSION/PROJECT_NAME` to get the executeable

You can directly run your project by using the `run` command and if you want you can pass params to the program

```console
$ synapse run ARGS
```

### Stage viewing
Using the `istage` command to see the files that have changed since last build. like so

```console
$ synapse istage
```

Well show every file that has changed with Syntax Highlighting to make it easier.

### Synapse Export Format (SEF)
Using the `export` command on a project will generate a `.sef` file

```console
$ synapse export
```

Using the `manifest` on the file generated will create a project exactly the same as the current project like so

```console
$ synapse manifest SEF_FILE
```

This is good for saving projects on your actual computer (Not GreyHack Game) so you can work on scripts through wipes without

being scared of loosing it. It's also a good way of sharing projects with other people.

## Synx
Synx is a system in Synapse that is good for project collabing, it involves a server and people pushing and pulling updates.

### Server Setup
You first want to add Synapse to the server and run `setup` command as `root`

```console
$ synapse synx setup SYNX_PASSWORD
```

The setup will create a folder `/synx` and an user called `synx` in the server with the password you passed

### Setting up Project
Back in your home computer you would want to create a directory and use the `init` command in the directory

```console
$ synapse synx init HOST_IP SYNX_PASSWORD HOST_PORT PROJECT_NAME
```

### Creating Project on Server
All you did if you setup the synx env is create configuration now you need to actually create the project on the server using the `make` command

```console
$ synapse synx make
```

That will create the project on the server

### Branches
Projects have branches the default branch is `master` when you use the `make` command it automatically makes a branch and a basic SEF file

You can create branches by using the `branch` command and passing it `create`

```console
$ synapse synx branch create BRANCH_NAME
```

You can create delete by using the `branch` command and passing it `delete`

```console
$ synapse synx branch delete BRANCH_NAME
```

Setting your current branch you're going to `push` and `pull` from by using the `set` option

```console
$ synapse synx branch set BRANCH_NAME
```

You can also list every branch a project has using the `list` option

```console
$ synapse synx branch list
```

### Pushing
Pushing means to convert the current project into a SEF and setting that branch's SEF

```console
$ synapse synx push
```

### Pulling
Pulling means to get the SEF file from the branch and initialize it in the current directory

```console
$ synapse synx pull
```

### Deleting Project
If you want to delete the project from the server you can just using the `delete` command

```console
$ synapse synx delete
```

## Personalization
After using Synapse once it will create a folder that's located in `/usr` which is called "`synapse`" Where stuff like

Console Syntax Highlighting colors can be changed or other coloring in `colors.conf`

`NOTE: MAKE SURE TO PASS HEX COLOR VALUES WHEN CHANGING A COLOR like this #FFFFFF`

And other settings.

## More help
You can use `help` command for a list of commands and use `synx help` for a list of `synx` commands

```console
$ synapse help
$ synapse synx help
```